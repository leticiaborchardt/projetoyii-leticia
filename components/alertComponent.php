<?
namespace app\components;

use yii\base\Component;

Class alertComponent extends Component{

    public static function myAlert($tipo, $mensagem, $url = ''){
        $component = "<div class=\"alert alert-{$tipo}\" role=\"alert\">{$mensagem}</div>";
        $componentParent = "
        <script type=\"text/javascript\">
            setTimeout(function(){
                $('main').find('div.alert').remove();
                if('{$url}'){
                    setTimeout(function(){
                        window.location.href = '{$url}';
                    }, 1000);
                }
            }, 2000)
        </script>
        ";

        return $component.$componentParent;
    }
}
?>