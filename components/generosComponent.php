<?
namespace app\components;

use yii\base\Component;

Class generosComponent extends Component {

    public static function generos($field,$default = false){

            $list = array(
                "M" => "Masculino",
                "F" => "Feminino",
                "O" => "Outros"
            );
            $estrutura = '';
            
            if($default != 'M' && $default != 'F' && $default){
                $estrutura .= "<label for=\"{$field}\">Gênero</label>";
                $defaultx = 'O';
                $estrutura .= "<select name=\"{$field}\" id=\"{$field}\" class=\"form-control shadow mb-1 bg-white rounded actionGenero\">
                <option value=\"\" selected >Selecione seu gênero</option>";
                foreach($list as $ch=>$g){
                    $estrutura .= "<option value=\"{$ch}\"".($ch == $defaultx ? ' selected' : '').">{$g}</option>";
                }
            
                $estrutura .= "</select>";
            if($default){
                $estrutura .= "<input class=\"form-control shadow mb-1 bg-white rounded outroGenero\" type=\"text\" name=\"{$field}\" value=\"{$default}\">";
            }
            
            return $estrutura;
        }
        else {          
            $estrutura .= "<label for=\"{$field}\">Gênero</label>";
            $estrutura .= "<select name=\"{$field}\" id=\"{$field}\" class=\"form-control shadow mb-1 bg-white rounded actionGenero\">
                <option value=\"\">Selecione seu gênero</option>";
                foreach($list as $ch=>$g){
                    $estrutura .= "<option value=\"{$ch}\"".($ch == $default ? ' selected' : '').">{$g}</option>";
                }
            
            $estrutura .= "</select>";
        }
            return $estrutura;
        }
    }
    ?>


