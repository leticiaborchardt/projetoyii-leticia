<?
namespace app\components;

use yii\base\Component;

class voceSabiaComponent extends Component {

    public static function gerarFrase(){
        $frases = array(
            "A lei nº4591/64 previa, no art.9º, alínea “j”, a obrigatoriedade da constituição do fundo reserva ao se instituir as Convenções. Com as alterações da lei 10406/02, o fundo reserva deixa de ser obrigatório se o assunto estiver omisso na Convenção.",

            "Não é o tamanho do condomínio tampouco as áreas de lazer que determinam o valor das cotas mensais pagas pelos moradores. Basicamente o que define este valor é o número de funcionários de cada prédio, uma vez que a folha de pagamento e encargos responde por cerca de 50% das despesas de um condomínio.",

            "Um condomínio precisa cuidar de seu Departamento Pessoal:
            contratar e demitir funcionários, pagar e demonstrar o pagamento dos encargos, e etc. Apesar de o condomínio não ser pessoa jurídica, ele possui obrigações de personalidade jurídica.",
            
            "A Convenção do Condomínio está para os proprietários das unidades condominiais como a Constituição Federal está para cada cidadão. É neste documento que devem estar detalhadas as normais gerais do condomínio, as obrigações, direitos, forma de cobrança de despesas e a conduta interna dos Condôminos.",
          
            "O síndico deve cumprir sua função com pulso firme. Sua atitude determina a agilidade com que serão resolvidos quaisquer contratempos que surgirem. Afinal, o síndico é quem assina embaixo das decisões tomadas por todos os condôminos e, nesse caso, a segurança com que o faz será decisiva."
        );
          
          // ordena o array randomicamente
          rand();
          shuffle ($frases);

          return $frases[0];
    }
}
?>