<?
namespace app\components;

use yii\base\Component;

Class maskComponent extends Component {

    public static function mask($val = '', $format){
        $maskared = '';
        $k = 0;
        switch ($format) {
            case 'cnpj':
                $mask = '##.###.###/####-##';
            break;
            
            case 'cpf':
                $mask = '###.###.###-##';
            break;

            case 'cep':
                $mask = '#####-##';
            break;

            case 'tel':
                $mask =  (strlen($val) > 10 ? '(##) #####-####' : '(##) ####-####');
            break;

            case 'taxa':
                $mask = 'R$ '.number_format($val, 2, ',', '.');
            break;

            default:
                $mask = null;
            break;
        }
        if($val == null){
            return "--";
        }

        for ($i = 0; $i <= strlen($mask) - 1; ++$i) { 
            if ($mask[$i] == '#') {
                if (isset($val[$k])) {
                    $maskared .= $val[$k++];
                }
            } else if (isset($mask[$i])) {
                $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public static function desMask($value){
        $value = str_replace(".", '', $value);
        $value = str_replace("-", '', $value);
        $value = str_replace("/", '', $value);
        $value = str_replace("(", '', $value);
        $value = str_replace(")", '', $value);
        $value = str_replace(" ", '', $value);
        
        return $value;
    }

    public static function money($value){
        $value = str_replace(",", '.', $value);
        $value = str_replace(" ", '', $value);
        $value = str_replace("R$", '', $value);

        return $value;
    }
}
?>