<?
use app\components\alertComponent;
use app\components\modalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'Listagem | Unidades';

$url_site = Url::base(true);

if (isset($_GET['myAlert'])) {
    echo alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg']);
}
?>

<h3 class="text-center mt-5 mb-4">Unidades Cadastradas</h3>
<div class="table-responsive">
    <table id="listaUnidade" class="table table-striped my-4 table-hover shadow bg-white rounded">
    <tr>
        <th scope="col">Condomínio</td> 
        <th scope="col">Bloco</td>
        <th scope="col">Nomenclatura</td>
        <th scope="col">Metragem (m²)</td>
        <th scope="col">Garagens</th>
        <th scope="col">Data Cadastro</th>
        <th scope="col" colspan="2">Ações</th>
    </tr>
    <?foreach ($unidades as $dadosUnidade) {?>
         <tr data-id="<?=$dadosUnidade['id']?>">
            <td><?=$dadosUnidade['nomeCondominio'] ?></td> 
            <td><?=$dadosUnidade['nomeBloco'] ?></td> 
            <td><?=$dadosUnidade['nomeUnidade'] ?></td>
            <td><?=$dadosUnidade['metragem'] ?></td>
            <td><?=$dadosUnidade['qtdGaragem'] ?></td>
            <td><?=yii::$app -> formatter -> format($dadosUnidade['dataCadastro'], 'date') ?></td>
            <td>
                <a class="text-dark h4 openModal" href="<?=$url_site?>/index.php?r=unidades/edita-unidade&id=<?=$dadosUnidade['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a class="text-dark h4"  href="<?=$url_site?>/index.php?r=unidades/deleta-unidade&id=<?=$dadosUnidade['id']?>"><i class="bi bi-trash3-fill"></i></a>
            <td>
        </tr>
    <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="?r=unidades/cadastro-unidade" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Unidade</a>
    </div>
    <div class="col-12 col-md-4 text-center d-flex justify-content-center">
        <?= LinkPager::widget([
            'pagination' => $paginacao,
            'linkContainerOptions' => ['class' => 'page-item'], 
            'linkOptions' => ['class' => 'page-link text-dark'],
            'disabledListItemSubTagOptions' => ['class' => 'page-link text-dark']
        ]) ?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros 
            <span class="badge badge-dark totalRegistros ">
                <?=$paginacao->totalCount?>
            </span>
        </p>
    </div>
</div>

<?=modalComponent::initModal();?>

