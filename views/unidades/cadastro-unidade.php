<?
use yii\helpers\Url;
use app\controllers\CondominiosController;

$this->title = 'Cadastrar | Unidade';

$url_site = Url::base($schema = true);
?>

<h3 class="text-center mt-5" >Cadastrar Unidades</h3>
<form id="cadastroUnidade" action="<?= Url::to(['unidades/realiza-cadastro-unidade'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="condominioBloco">A qual condomínio esta unidade pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCondominio" name="from_condominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>
        <div class="form-group col-md-4">
            <label for="condominioBloco">A qual bloco esta unidade pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" required>
           
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nomeUnidade">Nomenclatura da Unidade</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeUnidade" id="nomeUnidade" placeholder="Ex.: Apto 101"required>
        </div>
        <div class="form-group col-md-4">
            <label for="metragem">Metragem (m²)</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="metragem" id="metragem" required>
        </div>
        <div class="form-group col-md-4">
            <label for="qtdGaragem">Quantidade de Garagens</label>
            <input type="number" min="0" class="form-control shadow mb-3 bg-white rounded" name="qtdGaragem" id="qtdGaragem" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-outline-secondary" href="?r=unidades/lista-unidade" role="button">Ir para a listagem</a>
        </div>
    </div>
</form> 