<?
use app\components\selectedComponent;
use app\controllers\BlocosController;
use yii\helpers\Url;
use app\controllers\CondominiosController;

$url_site = Url::base(true);
?>

<h3 class="text-center mt-3" >Editar Unidade</h3>
<form id="cadastroUnidade" action="<?= Url::to(['unidades/realiza-edicao'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="condominioBloco">A qual condomínio esta unidade pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCondominio" name="from_condominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"<?=selectedComponent::isSelected($dado['id'], $edit['from_condominio'])?>><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="condominioBloco">A qual bloco esta unidade pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" required>
                <option value="" disabled selected>Selecione o Bloco</option>
                <? foreach(BlocosController::listaBlocoSelectFrom($edit['from_condominio']) as $dado){?>
                        <option value="<?=$dado['id']?>"<?=selectedComponent::isSelected($dado['id'], $edit['from_bloco'])?>><?=$dado['nomeBloco']?></option>
                <?}?>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nomeUnidade">Nomenclatura da Unidade</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeUnidade" id="nomeUnidade" placeholder="Ex.: Apto 101" value="<?=$edit['nomeUnidade']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="metragem">Metragem (m²)</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="metragem" id="metragem" value="<?=$edit['metragem']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="qtdGaragem">Quantidade de Garagens</label>
            <input type="number" min="0" class="form-control shadow mb-3 bg-white rounded" name="qtdGaragem" id="qtdGaragem" value="<?=$edit['qtdGaragem']?>" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Salvar Alterações</button>
            <a class="btn btn-sm btn-outline-secondary" data-dismiss="modal" role="button">Fechar</a>
        </div>
    </div>
</form>