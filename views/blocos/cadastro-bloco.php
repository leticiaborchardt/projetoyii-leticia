<?
use yii\helpers\Url;
use app\controllers\CondominiosController;

$this->title = 'Cadastrar | Bloco';

$url_site = Url::base($schema = true);
?>

<h3 class="text-center mt-5" >Cadastrar Blocos</h3>
<form id="cadastroBloco" action="<?= Url::to(['blocos/realiza-cadastro-bloco'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="condominioBloco">A qual condomínio este bloco pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_condominio"  required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nomeBloco">Nomenclatura do Bloco</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeBloco" id="nomeBloco" placeholder="Ex.: Bloco A / Bloco 01..."required>
        </div>
        <div class="form-group col-md-4">
            <label for="andares">Quantidade de Andares</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="andares" id="andares" required>
        </div>
        <div class="form-group col-md-4">
            <label for="qtdUnidades">Quantidade de Unidades por andar</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="qtdUnidades" id="qtdUnidades" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-outline-secondary" href="?r=blocos/lista-bloco" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>