<?
use app\components\selectedComponent;
use yii\helpers\Url;
use app\controllers\CondominiosController;

$url_site = Url::base(true);
?>

<h3 class="text-center mt-3" >Editar Bloco</h3>
<form id="cadastroBloco" action="<?= Url::to(['blocos/realiza-edicao'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="condominioBloco">A qual condomínio este bloco pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_condominio"  required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"<?=selectedComponent::isSelected($dado['id'], $edit['from_condominio'])?>><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nomeBloco">Nomenclatura do Bloco</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeBloco" id="nomeBloco" placeholder="Ex.: Bloco A / Bloco 01..." value="<?=$edit['nomeBloco']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="andares">Quantidade de Andares</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="andares" id="andares" value="<?=$edit['andares']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="qtdUnidades">Unidades por andar</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="qtdUnidades" id="qtdUnidades" value="<?=$edit['qtdUnidades']?>" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Salvar Alterações</button>
            <a class="btn btn-sm btn-outline-secondary" data-dismiss="modal" role="button">Fechar</a>
        </div>
    </div>
</form>