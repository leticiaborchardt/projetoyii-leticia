<?

use app\components\voceSabiaComponent;
use app\controllers\AdministradorasController;
use yii\helpers\Html;

$this->title = 'Home | LB Systems';
?>

<div class="row mb-3">
    <div class="col-12 breadcrumb-item active"><p>Home | DashBoard</p></div>
</div>
<div class="row no-gutters">
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 px-3 mb-4">
        <div class="divInfo shadow">
            <div class="divChild linearBlack shadow">
                <p class="text-white text-center pt-2 h5">
                    <?=$administradoras?>
                </p>
            </div>
            <div class="divChildText text-right">
                <p class="pt-2 pr-3 m-0 text-secondary">| Administradoras</p>
                <small class="pr-3 text-secondary">Cadastradas no sistema.</small>
            </div>
            <div class="text-left">
                <a href="?r=administradoras/lista-administradora" class="pl-3 pt-5 m-0 text-secondary"><small><u>Visualizar</u></small></a>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 px-3 mb-4">
        <div class="divInfo shadow">
            <div class="divChild linearRoxo shadow">
                <p class="text-white text-center pt-2 h5">
                    <?=$condominios?>
                </p>
            </div>
            <div class="divChildText text-right">
                <p class="pt-2 pr-3 m-0 text-secondary">| Condomínios</p>
                <small class="pr-3 text-secondary">Cadastrados no sistema.</small>
            </div>
            <div class="text-left">
                <a href="?r=condominios/lista-condominio" class="pl-3 pt-5 m-0 text-secondary"><small><u>Visualizar</u></small></a>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 px-3 mb-4">
        <div class="divInfo shadow">
            <div class="divChild linearAzul shadow">
                <p class="text-white text-center pt-2 h5">
                    <?=$moradores?>
                </p>
            </div>
            <div class="divChildText text-right">
                <p class="pt-2 pr-3 m-0 text-secondary">| Moradores</p>
                <small class="pr-3 text-secondary">Cadastrados no sistema.</small>
            </div>
            <div class="text-left">
                <a href="?r=moradores/lista-morador" class="pl-3 pt-5 m-0 text-secondary"><small><u>Visualizar</u></small></a>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-6 col-md-6 col-lg-3 px-3 mb-4">
        <div class="divInfo shadow">
            <div class="divChild linearRosa shadow">
                <p class="text-white text-center pt-2 h5">
                    <?=$unidades?>
                </p>
            </div>
            <div class="divChildText text-right">
                <p class="pt-2 pr-3 m-0 text-secondary">| Unidades</p>
                <small class="pr-3 text-secondary">Cadastradas no sistema.</small>
            </div>
            <div class="text-left">
                <a href="?r=unidades/lista-unidade" class="pl-3 pt-5 m-0 text-secondary"><small><u>Visualizar</u></small></a>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 px-3 mb-4 ">
        <div class="graficos shadow p-3 ml-2">
            <canvas id="grafico1"></canvas>
            <script>
                const labelsG1 = [
                'Segunda',
                'Terça',
                'Quarta',
                'Quinta',
                'Sexta',
                'Sábado',
                'Domingo',
                ];

                const dataG1 = {
                labels: labelsG1,
                datasets: [{
                    label: 'Cadastro de novas Unidades - Semanal',
                    backgroundColor: '#2570d9',
                    borderColor: '#2570d9',
                    data: [10, 5, 2, 5, 8, 4, 12],
                }]
                };

                const configG1 = {
                type: 'line',
                data: dataG1,
                options: {}
                };
            </script>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 px-3 mb-4 ">
        <div class="graficos shadow p-3 ml-2">
            <canvas id="grafico2"></canvas>
            <script>
                const labelsG2 = [
                'Condomínio',
                'Condomínio',
                'Condomínio',
                'Condomínio',
                ];

                const dataG2 = {
                labels: labelsG2,
                datasets: [{
                    label: 'Moradores por Condomínio',
                    backgroundColor: ['rgba(14,83,212,0.25)', 'rgba(0,69,198,0.41)'],
                    borderColor: 'blue',
                    data: [<?=$administradoras?>, 10, 8, 5],
                }]
                };

                const configG2 = {
                type: 'bar',
                data: dataG2,
                options: {}
                };
            </script>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12 col-sm-12 col-md-12 col-lg-7 px-3 mb-4">
        <div class="shadow infoNew p-4">
            <h4>
                <i class="bi bi-geo-alt"></i> 
                    Áreas Comuns 
                    <span class="badge badge-bg">Novo</span>
            </h4>
            <p class="text-secondary">Agora você pode gerenciar as áreas comuns do seu condomínio! Clique <a href="index.php?r=areas-comuns/cadastro-area"><u>aqui</u></a> para realizar seu primeiro cadastro.</p>
            <h4>
                <i class="bi bi-clipboard2-heart"></i>
                    Pets
                    <span class="badge badge-bg">Novo</span>
            </h4>
            <p class="text-secondary">Foi introduzido no sistema o cadastro e gerenciamento de pets do condomínio. Você pode realizar seu primeiro registro em Pets > Cadastrar Pets.</p>
        </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-5 px-3 mb-4">
        <div class="shadow infoNew p-4">
            <h4>
                <i class="bi bi-question-square"></i> Você Sabia?
            </h4>
            <p class="text-secondary text-justify"><?=voceSabiaComponent::gerarFrase()?></p>
        </div>
    </div>
</div>