<?
use app\components\alertComponent;
use yii\helpers\Url;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LB Systems | Login</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/login.css">
</head>

<body>

    <main class="container-fluid">
        <div class="row align-items-center">
            <div class="col-10 offset-1 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-4 offset-lg-4 col-xl-4 offset-xl-4 colForm shadow p-5 align-self-center"> 
                <div class="text-center mb-5">
                    <img src="img/logoSite.png" alt="" width="200px">
                    <p>Efetue seu login para acessar todas as funcionalidades do sistema.</p>
                </div>
                <form action="<?=Url::to(['site/login']);?>" class="my-3 text-dark" method="POST">
                
                    <?if (isset($_GET['myAlert'])) {
                        echo alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg'], $_GET['myAlert']['redir'] );
                    }?>
                    
                    <label for="usuario">Login Usuário</label>
                    <input type="text" name="usuario" id="usuario" class="form-control mb-4 shadow" placeholder="Digite seu usuário" required>

                    <label for="senha">Senha</label>
                    <input type="password" name="senha" id="senha" class="form-control shadow" placeholder="Digite sua senha" required>
                    <small ><a href="#"><i class="bi bi-lock-fill"></i> Esqueci minha senha.</a></small>

                    <input type="hidden" name="<?=\yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
                    <center><button type="submit" class="btn btn-outline-info mt-3 shadow">Realizar Login</button></center>   
                </form> 
            </div> 
    </main>
    
    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/app.js"></script>
</body>

</html>