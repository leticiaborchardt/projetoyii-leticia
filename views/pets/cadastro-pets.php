<?
use yii\helpers\Url;
use app\controllers\CondominiosController;

$this->title = 'Cadastrar | Pets';

$url_site = Url::base(true);
?>

<h3 class="text-center mt-5" >Cadastrar Pets</h3>
<form id="cadastroPets" action="<?= Url::to(['pets/realiza-cadastro-pets'])?>" class="my-5" method="post">
    <div class="form-row">

        <div class="form-group col-md-4">
        <label for="nomeCondominio">Condomínio</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCondominio" name="from_condominio" id="nomeCondominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>

        <div class="form-group col-md-4">
        <label for="nomeBloco">Bloco</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" id="nomeBloco" required>
            
            </select>
        </div>

        <div class="form-group col-md-4">
        <label for="nomeUnidade">Unidade</label>
            <select class="form-control shadow mb-3 bg-white rounded fromUnidade" name="from_unidade" id="nomeUnidade" required>
            
            </select>
        </div>

    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nome">Nome</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" required>
        </div>

        <div class="form-group col-md-4">
            <label for="especie">Espécie</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="especie" placeholder="Ex.: Gato/Cachorro" required>
        </div>

        <div class="form-group col-md-4">
            <label for="raca">Raça</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="raca" required>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="porte">Porte</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="porte"  required>
                <option value="" disabled selected>Selecione o porte</option>
                <option value="P">Pequeno</option>
                <option value="M">Médio</option>
                <option value="G">Grande</option>
            </select>
        </div>

        <div class="form-group col-md-6">
            <label for="cor">Cor</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="cor">
        </div>
    </div> 

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-outline-secondary" href="?r=pets/lista-pets" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>
