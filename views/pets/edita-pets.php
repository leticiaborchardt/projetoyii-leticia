<?
use yii\helpers\Url;
use app\controllers\CondominiosController;
use app\components\selectedComponent;
use app\controllers\BlocosController;
use app\controllers\UnidadesController;

$url_site = Url::base(true);
?>

<h3 class="text-center mt-3" >Editar Pets</h3>
<form id="cadastroPets" action="<?= Url::to(['pets/realiza-edicao'])?>" class="my-5" method="post">
    <div class="form-row">

        <div class="form-group col-md-6">
        <label for="nomeCondominio">Condomínio</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCondominio" name="from_condominio" id="nomeCondominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"<?=selectedComponent::isSelected($dado['id'], $edit['from_condominio'])?>><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>

        <div class="form-group col-md-3">
        <label for="nomeBloco">Bloco</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" id="nomeBloco" required>
                <option value="" disabled selected>Selecione o Bloco</option>
                <? foreach(BlocosController::listaBlocoSelectFrom($edit['from_condominio']) as $dado){?>
                        <option value="<?=$dado['id']?>"<?=selectedComponent::isSelected($dado['id'], $edit['from_bloco'])?>><?=$dado['nomeBloco']?></option>
                <?}?>
            </select>
        </div>

        <div class="form-group col-md-3">
        <label for="nomeUnidade">Unidade</label>
            <select class="form-control shadow mb-3 bg-white rounded fromUnidade" name="from_unidade" id="nomeUnidade" required>
            <option value="" disabled selected>Selecione a Unidade</option>
                <? foreach(UnidadesController::listaUnidadeSelectFrom($edit['from_bloco']) as $dado){?>
                        <option value="<?=$dado['id']?>"<?=selectedComponent::isSelected($dado['id'], $edit['from_unidade'])?>><?=$dado['nomeUnidade']?></option>
                <?}?>
            </select>
        </div>

    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nome">Nome</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" value="<?=$edit['nome']?>" required>
        </div>

        <div class="form-group col-md-4">
            <label for="especie">Espécie</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="especie" placeholder="Ex.: Gato/Cachorro" value="<?=$edit['especie']?>" required>
        </div>

        <div class="form-group col-md-4">
            <label for="raca">Raça</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="raca" value="<?=$edit['raca']?>" required>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="porte">Porte</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="porte"  required>
                <option value="" disabled selected>Selecione o porte</option>
                <option value="P" <?=('P' == $edit['porte'] ? 'selected' : '')?>>Pequeno</option>
                <option value="M" <?=('M' == $edit['porte'] ? 'selected' : '')?>>Médio</option>
                <option value="G" <?=('G' == $edit['porte'] ? 'selected' : '')?>>Grande</option>
            </select>
        </div>

        <div class="form-group col-md-6">
            <label for="cor">Cor</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="cor" value="<?=$edit['cor']?>">
        </div>
    </div> 

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Salvar Alterações</button>
            <a class="btn btn-sm btn-outline-secondary" data-dismiss="modal" role="button">Fechar</a>
        </div>
    </div>
</form>
