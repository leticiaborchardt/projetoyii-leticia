<?
use yii\helpers\Url;
use app\controllers\CondominiosController;
use app\components\generosComponent;

$this->title = 'Cadastrar | Morador';

$url_site = Url::base(true);
?>

<h3 class="text-center mt-5" >Cadastrar Moradores</h3>
<form id="cadastroMorador" action="<?= Url::to(['moradores/realiza-cadastro-morador'])?>" class="my-5" method="post">
    <div class="form-row">

        <div class="form-group col-md-4">
        <label for="nomeCondominio">Condomínio</label>
            <select class="form-control shadow mb-3 bg-white rounded fromCondominio" name="from_condominio" id="nomeCondominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>

        <div class="form-group col-md-4">
        <label for="nomeBloco">Bloco</label>
            <select class="form-control shadow mb-3 bg-white rounded fromBloco" name="from_bloco" id="nomeBloco" required>
            
            </select>
        </div>

        <div class="form-group col-md-4">
        <label for="nomeUnidade">Unidade</label>
            <select class="form-control shadow mb-3 bg-white rounded fromUnidade" name="from_unidade" id="nomeUnidade" required>
            
            </select>
        </div>

    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nome">Nome completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" id="nome" required>
        </div>

        <div class="form-group col-md-4">
            <?=generosComponent::generos('genero')?>
        </div>

        <div class="form-group col-md-4">
            <label for="cpf">CPF</label>
            <input type="text" maxlength="11" minlength="11" class="form-control shadow mb-3 bg-white rounded" name="cpf" id="cpf" required>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="email">E-mail</label>
            <input type="email" class="form-control shadow mb-3 bg-white rounded" name="email" id="email" required>
        </div>
        <div class="form-group col-md-6">
            <label for="tel">Telefone/Celular</label>
            <input type="text" maxlength="11" class="form-control shadow mb-3 bg-white rounded" name="telefone" id="tel">
        </div>
    </div> 

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-outline-secondary" href="?r=moradores/lista-morador" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>
