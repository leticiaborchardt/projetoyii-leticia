<?
use app\components\alertComponent;
use app\components\maskComponent;
use app\components\modalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'Listagem | Moradores';

$url_site = Url::base(true);

if (isset($_GET['myAlert'])) {
    echo alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg']);
}
?>

<h3 class="text-center mt-5 mb-4">Moradores Cadastrados</h3>
<div class="table-responsive">
    <table id="listaMoradores" class="table table-striped my-3 table-hover shadow bg-white rounded">
        <tr>
            <th scope="col">Condomínio</th>
            <th scope="col">Bloco</th>
            <th scope="col">Unidade</th> 
            <th scope="col">Nome Completo</th>
            <th scope="col">CPF</th>
            <th scope="col">E-mail</th>
            <th scope="col">Telefone</th>
            <th scope="col">Data Cadastro</th>
            <th scope="col" colspan="2">Ações</th>
        </tr>
        <? foreach ($moradores as $dados) { ?> 
            <tr data-id="<?= $dados['id'] ?>">
                <td><?= $dados['nomeCondominio'] ?></td>
                <td><?= $dados['nomeBloco'] ?></td>
                <td><?= $dados['nomeUnidade'] ?></td> 
                <td><?= $dados['nome'] ?></td>
                <td><?=maskComponent::mask($dados['cpf'], 'cpf')?></td>
                <td><?= $dados['email'] ?></td>
                <td><?=maskComponent::mask($dados['telefone'], 'tel') ?></td>
                <td><?= yii::$app -> formatter -> format($dados['dataCadastro'], 'date') ?></td>
                <td>
                    <a class="text-dark h4 openModal" href="<?=$url_site?>/index.php?r=moradores/edita-morador&id=<?=$dados['id']?>"><i class="bi bi-pencil-square"></i></a>
                    <a class="text-dark h4" href="<?=$url_site?>/index.php?r=moradores/deleta-morador&id=<?=$dados['id']?>"><i class="bi bi-trash3-fill"></i></a>
                <td>
            </tr>
        <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="?r=moradores/cadastro-morador" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Morador</a>
    </div>
    <div class="col-12 col-md-4 text-center d-flex justify-content-center">
        <?= LinkPager::widget([
            'pagination' => $paginacao,
            'linkContainerOptions' => ['class' => 'page-item'], 
            'linkOptions' => ['class' => 'page-link text-dark'],
            'disabledListItemSubTagOptions' => ['class' => 'page-link text-dark']
        ]) ?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros
            <span class="badge badge-dark totalRegistros ">
                <?= $paginacao->totalCount ?>
            </span>
        </p>
    </div>
</div>

<?=modalComponent::initModal();?>