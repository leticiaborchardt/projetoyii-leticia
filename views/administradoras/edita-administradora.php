<?
use app\components\maskComponent;
use yii\helpers\Url;
?>

<h3 class="text-center mt-3" >Editar Administradora</h3>
<form id="cadastroAdm" action="<?= Url::to(['administradoras/realiza-edicao'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nome">Nome da Administradora</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" id="nome" value="<?=$edit['nome']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="cnpj">CNPJ</label>
            <input type="text" maxlength="14" class="form-control shadow mb-3 bg-white rounded" name="cnpj" id="cnpj" value="<?=maskComponent::mask($edit['cnpj'], 'cnpj') ?>" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Salvar Alterações</button>
            <a class="btn btn-sm btn-outline-secondary" data-dismiss="modal" role="button">Fechar</a>
        </div>
    </div>
</form>