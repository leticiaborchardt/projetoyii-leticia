<?
use app\components\alertComponent;
use app\components\maskComponent;
use app\components\modalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'Listagem | Administradoras';

$url_site = Url::base($schema = true);

if (isset($_GET['myAlert'])) {
    echo alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg']);
}
?>

<h3 class="text-center mt-5 mb-4">Administradoras Cadastradas</h3>
<div class="table-responsive">
    <table id="listaAdm" class="table table-striped my-4 table-hover shadow bg-white rounded">
        <tr>
            <th scope="col">Nome</td>
            <th scope="col">CNPJ</td>
            <th scope="col">Data Cadastro</th>
            <th scope="col" colspan="2">Ações</th>
        </tr>
        <? foreach ($administradoras as $dadosAdm) { ?>
            <tr data-id="<?= $dadosAdm['id'] ?>">
                <td><?= $dadosAdm['nome'] ?></td>
                <td><?= maskComponent::mask($dadosAdm['cnpj'], 'cnpj') ?></td>
                <td><?= yii::$app -> formatter -> format($dadosAdm['dataCadastro'], 'date') ?></td>
                <td>
                    <a class="text-dark h4 openModal" href="<?=$url_site?>/index.php?r=administradoras/edita-administradora&id=<?= $dadosAdm['id'] ?>"><i class="bi bi-pencil-square"></i></a>
                    <a class="text-dark h4" href="<?=$url_site?>/index.php?r=administradoras/deleta-administradora&id=<?= $dadosAdm['id'] ?>"><i class="bi bi-trash3-fill"></i></a>
                <td>
            </tr>
        <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="?r=administradoras/cadastro-administradora" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Administradora</a>
    </div>
    <div class="col-12 col-md-4 text-center d-flex justify-content-center">
        <?= LinkPager::widget([
            'pagination' => $paginacao,
            'linkContainerOptions' => ['class' => 'page-item'], 
            'linkOptions' => ['class' => 'page-link text-dark'],
            'disabledListItemSubTagOptions' => ['class' => 'page-link text-dark']
        ]) ?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros
            <span class="badge badge-dark totalRegistros ">
                <?= $paginacao->totalCount ?>
            </span>
        </p>
    </div>
</div>

<?=modalComponent::initModal();?>