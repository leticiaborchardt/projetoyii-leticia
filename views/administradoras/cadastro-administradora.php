<?
use yii\helpers\Url;

$this->title = 'Cadastrar | Administradora';

$url_site = Url::base($schema = true);
?>

<h3 class="text-center mt-5" >Cadastrar Administradoras</h3>
<form id="cadastroAdm" action="<?= Url::to(['administradoras/realiza-cadastro-administradora'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nome">Nome da Administradora</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" id="nome" required>
        </div>
        <div class="form-group col-md-4">
            <label for="cnpj">CNPJ</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="cnpj" id="cnpj" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-outline-secondary" href="?r=administradoras/lista-administradora" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>