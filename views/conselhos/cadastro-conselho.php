<?
use yii\helpers\Url;
use app\controllers\CondominiosController;

$this->title = 'Cadastrar | Conselho';

$url_site = Url::base($schema = true);
?>

<h3 class="text-center mt-5" >Cadastrar Conselho</h3>
<form id="cadastroConselho" action="<?= Url::to(['conselhos/realiza-cadastro-conselho'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="condominioBloco">A qual condomínio este conselho pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_condominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div> 
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nomeSindico">Nome Completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" id="nomeSindico" required>
        </div>
        <div class="form-group col-md-4">
            <label for="nomeSindico">Função</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_funcao"  required>
                <option value="" disabled selected>Selecione a Função</option>
                <option value="Subsíndico">Subsíndico(a)</option>
                <option value="Conselheiro">Conselheiro(a)</option>
            </select>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="?r=conselhos/lista-conselho" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>