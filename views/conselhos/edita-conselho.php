<?
use app\components\selectedComponent;
use yii\helpers\Url;
use app\controllers\CondominiosController;

$url_site = Url::base(true);
?>

<h3 class="text-center mt-3" >Editar Conselho</h3>
<form id="cadastroConselho" action="<?= Url::to(['conselhos/realiza-edicao'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="condominioBloco">A qual condomínio este conselho pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_condominio" required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"<?=selectedComponent::isSelected($dado['id'], $edit['from_condominio'])?>><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div> 
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nomeSindico">Nome Completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" value="<?=$edit['nome']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="nomeSindico">Função</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_funcao" required>
                <option value="" disabled selected>Selecione a Função</option>
                <option value="Subsíndico" <?=('Subsíndico' == $edit['from_funcao'] ? 'selected' : '')?>>Subsíndico(a)</option>
                <option value="Conselheiro" <?=('Conselheiro' == $edit['from_funcao'] ? 'selected' : '')?>>Conselheiro(a)</option>
            </select>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2 buttonEnviar">Salvar Alterações</button>
            <a class="btn btn-sm btn-secondary" data-dismiss="modal" role="button">Fechar</a>
        </div>
    </div>
</form>