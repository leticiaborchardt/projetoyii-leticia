<?

use app\components\maskComponent;
use app\components\selectedComponent;
use yii\helpers\Url;
use app\controllers\CondominiosController;

$url_site = Url::base(true);
?>

<h3 class="text-center mt-5" >Editar Área Comum</h3>
<form id="cadastroArea" action="<?= Url::to(['areas-comuns/realiza-edicao'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="areaCondo">A qual condomínio esta área pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_condominio"  required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"<?=selectedComponent::isSelected($dado['id'], $edit['from_condominio'])?>><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="nome">Nome da Área</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" required placeholder="Ex.: Salão de Festas" value="<?=$edit['nome']?>">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="metragem">Metragem (m²)</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="metragem" required value="<?=$edit['metragem']?>">
        </div>
        <div class="form-group col-md-4">
            <label for="taxa">Taxa de Reserva (opcional)</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="taxa" id="maskMoney" required value="<?=maskComponent::mask($edit['taxa'], 'taxa') ?>"> 
        </div>
        <div class="form-group col-md-4">
            <label for="lotacaoMax">Lotação Máxima</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="lotacaoMax" required value="<?=$edit['lotacaoMax']?>">
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Salvar Alterações</button>
            <a class="btn btn-sm btn-outline-secondary" data-dismiss="modal" role="button">Fechar</a>
        </div>
    </div>
</form>