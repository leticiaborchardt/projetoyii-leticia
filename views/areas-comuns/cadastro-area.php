<?

use app\controllers\CondominiosController;
use yii\helpers\Url;

$this->title = 'Cadastrar | Área Comum';

$url_site = Url::base($schema = true);
?>

<h3 class="text-center mt-5" >Cadastrar Área Comum</h3>
<form id="cadastroArea" action="<?= Url::to(['areas-comuns/realiza-cadastro-area'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="areaCondo">A qual condomínio esta área pertence?</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_condominio"  required>
                <option value="" disabled selected>Selecione o Condomínio</option>
                <? foreach(CondominiosController::listaCondominioSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"><?=$dado['nomeCondominio']?></option>
                <?}?>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="nome">Nome da Área</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" required placeholder="Ex.: Salão de Festas">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="metragem">Metragem (m²)</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="metragem" required>
        </div>
        <div class="form-group col-md-4">
            <label for="taxa">Taxa de Reserva (opcional)</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="taxa" id="maskMoney">
        </div>
        <div class="form-group col-md-4">
            <label for="lotacaoMax">Lotação Máxima</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="lotacaoMax" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    
    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-outline-secondary" href="?r=areas-comuns/lista-area" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>