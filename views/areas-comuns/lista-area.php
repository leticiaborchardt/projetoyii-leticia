<?
use app\components\alertComponent;
use app\components\maskComponent;
use app\components\modalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'Listagem | Áreas Comuns';

$url_site = Url::base(true);

if (isset($_GET['myAlert'])) {
    echo alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg']);
}
?>

<h3 class="text-center mt-5 mb-4">Áreas Comuns Cadastradas</h3>
<div class="table-responsive">
    <table id="listaArea" class="table table-striped my-4 table-hover shadow bg-white rounded">
    <tr>
        <th scope="col">Condomínio</td>
        <th scope="col">Área Comum</td>
        <th scope="col">Metragem</td>
        <th scope="col">Taxa de Reserva</td>
        <th scope="col">Lotação Máxima</td>
        <th scope="col">Data Cadastro</th>
        <th scope="col" colspan="2">Ações</th>
    </tr>
    <? foreach ($areas as $dadosArea) {?>
        <tr data-id="<?=$dadosArea['id']?>">
            <td><?=$dadosArea['nomeCondominio'] ?></td>
            <td><?=$dadosArea['nome'] ?></td>
            <td><?=$dadosArea['metragem'] ?></td>
            <td><?=maskComponent::mask($dadosArea['taxa'], 'taxa') ?></td>
            <td><?=$dadosArea['lotacaoMax'] ?></td>
            <td><?=yii::$app -> formatter -> format($dadosArea['dataCadastro'], 'date') ?></td>
            <td>
                <a class="text-dark h4 openModal" href="<?=$url_site?>/index.php?r=areas-comuns/edita-area&id=<?=$dadosArea['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a class="text-dark h4" href="<?=$url_site?>/index.php?r=areas-comuns/deleta-area&id=<?=$dadosArea['id']?>"><i class="bi bi-trash3-fill"></i></a>
            <td>
        </tr>
    <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="?r=areas-comuns/cadastro-area" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Área Comum</a>
    </div>
    <div class="col-12 col-md-4 text-center d-flex justify-content-center">
        <?= LinkPager::widget([
            'pagination' => $paginacao,
            'linkContainerOptions' => ['class' => 'page-item'], 
            'linkOptions' => ['class' => 'page-link text-dark'],
            'disabledListItemSubTagOptions' => ['class' => 'page-link text-dark']
        ]) ?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros 
            <span class="badge badge-dark totalRegistros ">
                <?= $paginacao->totalCount ?>
            </span>
        </p>
    </div>
</div>

<?=modalComponent::initModal();?>