<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use kartik\icons\FontAwesomeAsset;

FontAwesomeAsset::register($this);
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="css/dashboard.css">
    <link rel="stylesheet" href="icofont.min.css">
    
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="d-flex flex-column h-100 bg-light">
    <?php $this->beginBody() ?>

    <nav class="navbar navbar-dark bg-dark flex-md-nowrap p-0 shadow">
        <a class="col-md-3 col-lg-2 mr-0 px-3 text-right text-dark p-1 ml-auto" href="#"><img src="img/logoSemEscrita.png" alt="" width="30px"></a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <i class="bi bi-list text-white h3"></i>
        </button>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <!-- SIDE BAR -->
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block sidebar collapse shadowNav">
                <div class="sidebar-sticky pt-3 h-100 ">
                    <div class="text-center">
                        <img src="img/logoSite.png" alt="" width="150px">
                    </div>
                    <ul class="nav flex-column ulNav">
                        <li class="nav-item">
                            <a class="nav-link active text-dark font-weight-bold btn-light" href="index.php">
                                <i class="bi bi-house h4"></i> HOME
                            </a>
                        </li>
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="dropdown-toggle text-dark nav-link" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="bi bi-bank h4"></i> Administradoras
                                </a>
                                <div class="dropdown-menu" aria-labelledby="drop">
                                    <a class="dropdown-item" href="?r=administradoras/lista-administradora">Listar Administradoras</a>
                                    <a class="dropdown-item" href="?r=administradoras/cadastro-administradora">Cadastrar Administradoras</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="dropdown-toggle text-dark nav-link" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="bi bi-building h4"></i> Condomínios
                                </a>
                                <div class="dropdown-menu" aria-labelledby="drop">
                                    <a class="dropdown-item" href="?r=condominios/lista-condominio">Listar Condomínios</a>
                                    <a class="dropdown-item" href="?r=condominios/cadastro-condominio">Cadastrar Condomínios</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="dropdown-toggle text-dark nav-link" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="bi bi-box h4"></i> Blocos
                                </a>
                                <div class="dropdown-menu" aria-labelledby="drop">
                                    <a class="dropdown-item" href="?r=blocos/lista-bloco">Listar Blocos</a>
                                    <a class="dropdown-item" href="?r=blocos/cadastro-bloco">Cadastrar Blocos</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="dropdown-toggle text-dark nav-link" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="bi bi-house-door h4"></i> Unidades
                                </a>
                                <div class="dropdown-menu" aria-labelledby="drop">
                                    <a class="dropdown-item" href="?r=unidades/lista-unidade">Listar Unidades</a>
                                    <a class="dropdown-item" href="?r=unidades/cadastro-unidade">Cadastrar Unidades</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="dropdown-toggle text-dark nav-link" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="bi bi-people h4"></i> Moradores
                                </a>
                                <div class="dropdown-menu" aria-labelledby="drop">
                                    <a class="dropdown-item" href="?r=moradores/lista-morador">Listar Moradores</a>
                                    <a class="dropdown-item" href="?r=moradores/cadastro-morador">Cadastrar Moradores</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="dropdown-toggle text-dark nav-link" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="bi bi-geo-alt h4"></i> Áreas Comuns
                                </a>
                                <div class="dropdown-menu" aria-labelledby="drop">
                                    <a class="dropdown-item" href="?r=areas-comuns/lista-area">Listar Áreas Comuns</a>
                                    <a class="dropdown-item" href="?r=areas-comuns/cadastro-area">Cadastrar Áreas Comuns</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="dropdown-toggle text-dark nav-link" href="#" role="button" id="drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="bi bi-clipboard2-heart h4"></i> Pets
                                </a>
                                <div class="dropdown-menu" aria-labelledby="drop">
                                    <a class="dropdown-item" href="?r=pets/lista-pets">Listar Pets</a>
                                    <a class="dropdown-item" href="?r=pets/cadastro-pets">Cadastrar Pets</a>
                                </div>
                            </div>
                        </li>

                        <li class="nav-item mt-5">
                        <?php
                            echo Nav::widget([
                                'options' => ['class' => 'nav-item text-nowrap text-dark'],
                                'items' => [
                                    Yii::$app->user->isGuest ? (['label' => 'Login', 'url' => ['/site/login']]
                                    ) : ('<li>'
                                        . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                                        . Html::submitButton(
                                            '<i class="bi bi-box-arrow-right h5"></i> Logout (' . Yii::$app->user->identity->usuario . ')',
                                            ['class' => 'btn btn-link text-dark logout']
                                        )
                                        . Html::endForm()
                                        . '</li>'
                                    )
                                ],
                            ]);
                            ?>
                        </li>

                    </ul>
                </div>
            </nav>
            <!-- END SIDE BAR -->

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                <div class="container-fluid pt-3 ">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>
            </main>
        </div>
    </div>

    <footer class="footer pt-3 text-muted bg-dark shadow">
        <div class="container">
            <p class="float-left">&copy; LB Systems | Todos os direitos reservados.</p>
        </div>
    </footer>
    
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>