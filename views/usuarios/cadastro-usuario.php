<?
use yii\helpers\Url;

$this->title = 'Cadastrar | Usuário';

$url_site = Url::base($schema = true);
?>

<h3 class="text-center mt-5" >Cadastrar Usuário no sistema</h3>
<form id="cadastroUsuario" action="<?= Url::to(['usuarios/realiza-cadastro-usuario'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nome">Nome Completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" id="nome" required>
        </div>
        <div class="form-group col-md-4">
            <label for="usuario">Usuário</label> <small>*Este será o login do sistema</small>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="usuario" id="usuario" required>
           
        </div>
        <div class="form-group col-md-6">
            <label for="senha">Senha</label>
            <input type="password" class="form-control shadow mb-3 bg-white rounded" name="senha" id="senha" required>
        </div>
        <div class="form-group col-md-6">
            <label for="csenha">Confirme sua Senha</label>
            <input type="password" class="form-control shadow mb-3 bg-white rounded" name="csenha" id="csenha" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">

    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-secondary" href="?r=usuarios/lista-usuario" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>