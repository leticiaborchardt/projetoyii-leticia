<?
use yii\helpers\Url;

$url_site = Url::base(true);
?>

<h3 class="text-center mt-3" >Editar Usuário do sistema</h3>
<form id="cadastroUsuario" action="<?= Url::to(['usuarios/realiza-edicao'])?>" class="my-5" method="post">
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="nome">Nome Completo</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nome" id="nome" value="<?=$edit['nome']?>" required>
        </div>
        <div class="form-group col-md-4">
            <label for="usuario">Usuário</label> <small>*Este será o login do sistema</small>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="usuario" id="usuario" value="<?=$edit['usuario']?>" required>
           
        </div>
        <div class="form-group col-md-6">
            <label for="senha">Senha</label>
            <input type="password" class="form-control shadow mb-3 bg-white rounded" name="senha" id="senha" required>
        </div>
        <div class="form-group col-md-6">
            <label for="csenha">Confirme sua Senha</label>
            <input type="password" class="form-control shadow mb-3 bg-white rounded" name="csenha" id="csenha" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    <input type="hidden" name="id" value="<?=$edit['id']?>">

    <div class="row">
        <div class="col-12">
            <button type="submit" class="btn btn-success mr-2 buttonEnviar">Salvar Alterações</button>
            <a class="btn btn-sm btn-secondary" data-dismiss="modal" role="button">Fechar</a>
        </div>
    </div>
</form>