<?
use app\components\alertComponent;
use app\components\modalComponent;
use yii\widgets\LinkPager;
use yii\helpers\Url;

$this->title = 'Listagem | Usuários';

$url_site = Url::base(true);

if (isset($_GET['myAlert'])) {
    echo alertComponent::myAlert($_GET['myAlert']['type'], $_GET['myAlert']['msg']);
}
?>

<h3 class="text-center mt-5 mb-4">Usuários Cadastradas</h3>
<div class="table-responsive">
    <table id="listaUsuario" class="table table-striped my-4 table-hover shadow bg-white rounded">
    <tr>
        <th scope="col">Nome Completo</td>
        <th scope="col">Usuario de acesso</td>
        <th scope="col">Data Cadastro</th>
        <th scope="col" colspan="2">Ações</th>
    </tr>
    <? foreach ($usuarios as $dado) { ?> 
        <tr data-id="<?=$dado['id']?>">
            <td><?=$dado['nome'] ?></td>
            <td><?=$dado['usuario'] ?></td>
            <td><?=yii::$app -> formatter -> format($dado['dataCadastro'], 'date') ?></td>
            <td>
                <a class="text-dark h4 openModal" href="<?=$url_site?>/index.php?r=usuarios/edita-usuario&id=<?=$dado['id']?>"><i class="bi bi-pencil-square"></i></a>
                <a class="text-dark h4" href="<?=$url_site?>/index.php?r=usuarios/deleta-usuario&id=<?=$dado['id']?>" ><i class="bi bi-trash3-fill"></i></a>
            <td>
        </tr>
    <? } ?>
    </table>
</div>
<div class="row mb-5">
    <div class="col-12 col-md-4">
        <a href="?r=usuarios/cadastro-usuario" class="text-left text-dark h6"><i class="bi bi-plus-circle-fill"></i> Adicionar Usuário</a>
    </div>
    <div class="col-12 col-md-4 text-center d-flex justify-content-center">
        <?= LinkPager::widget([
            'pagination' => $paginacao,
            'linkContainerOptions' => ['class' => 'page-item'], 
            'linkOptions' => ['class' => 'page-link text-dark'],
            'disabledListItemSubTagOptions' => ['class' => 'page-link text-dark']
        ]) ?>
    </div>
    <div class="col-12 col-md-4">
        <p class="text-right ">Total de registros 
            <span class="badge badge-dark totalRegistros ">
                <?= $paginacao->totalCount ?>
            </span>
        </p>
    </div>
</div>

<?=modalComponent::initModal();?>