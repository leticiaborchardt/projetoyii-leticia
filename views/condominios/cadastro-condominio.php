<?
use yii\helpers\Url;
use app\controllers\AdministradorasController;
use app\components\estadosComponent;

$this->title = 'Cadastrar | Condomínio';

$url_site = Url::base($schema = true);

?>

<h3 class="text-center mt-5" >Cadastrar Condomínios</h3>
<form id="cadastroCondominio" action="<?= Url::to(['condominios/realiza-cadastro-condominio'])?>" method="post">
    <div class="form-row mt-4">
        <div class="form-group col-md-12">
            <label for="from_administradora">Administradora</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="from_administradora" required>
                <option value="" >Selecione a Administradora</option>
                <? foreach(AdministradorasController::listaAdministradoraSelect() as $dado){?>
                        <option value="<?=$dado['id']?>"><?=$dado['nome']?></option>
                <?}?>
            </select>
        </div>
    </div>

    <div class="form-row mt-4">
        <div class="form-group col-md-9">
            <label for="nomeCondominio">Nome do condomínio</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="nomeCondominio" id="nomeCondominio"  required>
        </div>
        <div class="form-group col-md-3">
            <label for="qtdBlocos">Quantidade de Blocos</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="qtdBlocos" id="qtdBlocos"  required>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <h5>Endereço</h5>
        </div>
        <div class="form-group col-md-3">
            <label for="cep">CEP</label>
            <input type="text" maxlength="8" class="form-control shadow mb-3 bg-white rounded" name="cep" id="cep">
        </div>
        <div class="form-group col-md-3">
            <label for="estado">Estado</label>
            <select class="form-control shadow mb-3 bg-white rounded" name="estado" id="estado">
                <option value="" disabled selected>Selecione</option>
                <?
                foreach(estadosComponent::estados() as $sig=>$uf) {
                   echo '<option id="uf" value="'.$sig.'"'.'>'.$uf.' </option>';}
                ?>
            </select>
        </div> 
        <div class="form-group col-md-6">
           <label for="cidade">Cidade</label>
           <input type="text" class="form-control shadow mb-3 bg-white rounded" name="cidade" id="cidade">
       </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="bairro">
        </div>
        <div class="form-group col-md-6">
            <label for="lougradouro">Lougradouro</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="lougradouro" id="lougradouro" >
        </div>
        <div class="form-group col-md-2">
            <label for="numero">Número</label>
            <input type="number" min="1" class="form-control shadow mb-3 bg-white rounded" name="numero" >
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-12">
            <h5>Síndico</h5>
        </div>
        <div class="form-group col-md-12">
            <label for="sindico">Nome completo do síndico(a)</label>
            <input type="text" class="form-control shadow mb-3 bg-white rounded" name="sindico" id="sindico" required>
        </div>
    </div>

    <input type="hidden" name="<?= \yii::$app->request->csrfParam;?>" value="<?= \yii::$app->request->csrfToken;?>">
    
    <div class="row mb-5">
        <div class="col-12 mb-4">
            <button type="submit" class="btn btn-info mr-2 buttonEnviar">Cadastrar</button>
            <a class="btn btn-sm btn-outline-secondary" href="?r=condominios/lista-condominio" role="button">Ir para a listagem</a>
        </div>
    </div>
</form>

