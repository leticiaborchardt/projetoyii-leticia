$(function () {
  
  // chama bloco após selecionar o condomínio
  $(document).on('change', '.fromCondominio',function() {
    selecionado = $(this).val();

    $.ajax({
      url: "?r=blocos/lista-bloco-api",
      dataType: "json",
      type: "POST",
      data: { id: selecionado },
      success: function (data) {
        selectPopulation(".fromBloco", data, "nomeBloco");
      },
    });
  });

  // chama area comum após selecionar o condomínio
  $(document).on('change', '.fromCondominio',function() {
    selecionado = $(this).val();

    $.ajax({
      url: "?r=areas-comuns/lista-area-api",
      dataType: "json",
      type: "POST",
      data: { id: selecionado },
      success: function (data) {
        selectPopulation(".fromArea", data, "nome");
      },
    });
  });


  // chama unidade após selecionar o bloco
  $(document).on('change', '.fromBloco', function() {
    selecionado = $(this).val();

    $.ajax({
      url: "?r=unidades/lista-unidade-api",
      dataType: "json",
      type: "POST",
      data: { id: selecionado },
      success: function (data) {
        selectPopulation(".fromUnidade", data, "nomeUnidade");
      },
    });
  });

    // chama morador após selecionar a unidade
    $(document).on('change', '.fromUnidade', function() {
      selecionado = $(this).val();
  
      $.ajax({
        url: "?r=moradores/lista-morador-api",
        dataType: "json",
        type: "POST",
        data: { id: selecionado },
        success: function (data) {
          selectPopulation(".fromMorador", data, "nome");
        },
      });
    });


  function selectPopulation(seletor, dados, field) {
    estrutura = '<option value:"">Selecione...</option>';
    for (let i = 0; i < dados.length; i++) {
      estrutura +=
        '<option value="' + dados[i].id + '">' + dados[i][field] + "</option>";
    }
    $(seletor).html(estrutura);
  }


  // Controle do modal
  $(".openModal").click(function () {
    caminho = $(this).attr("href");
    $(".modal-body").load(caminho, function (response, status) {
      if (status === "success") {
        $("#modalComponent").modal({ show: true });
      }
    });

    return false;
  });
  
  
  //mascaras
  $('#maskMoney').maskMoney({
    prefix:'R$ ',
    allowNegative: true,
    thousands:'.', decimal:',',
    affixesStay: true});

  $('input[name="cpf"]').mask("000.000.000-00");
  $('input[name="cnpj"]').mask("00.000.000/0000-00");
  $('input[name="telefone"]').mask("(00) 0000-0000");
  $('input[name="cep"]').mask("00000-000");

  var behavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
  },
    options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior.apply({}, arguments), options);
        }
    };

  $('input[name="telefone"]').mask(behavior, options);



  // API BUSCAR ENDEREÇO
    // Preenchimento automático a partir do CEP
    function limpaFormulário() {
      $('select[name="estado"]').val("");
      $('input[name="cidade"]').val("");
      $('input[name="bairro"]').val("");
      $('input[name="lougradouro"]').val("");
  }

  $("#cep").blur(function () {
      var cep = $("#cep").val().replace(/[^0-9]/g, ''); // Formata o cep apenas com numeros

      if (cep != "") {

          $.ajax({
              url: "https://viacep.com.br/ws/" + cep + "/json/",
              type: "GET",
              dataType: "json",
              success: function (data) {
                  if (data.erro !== undefined) {
                      limpaFormulário();
                      alert("CEP não encontrado.");
                  } else {
                      $('select[name="estado"]').val(data.uf);
                      $('input[name="cidade"]').val(data.localidade);
                      $('input[name="bairro"]').val(data.bairro);
                      $('input[name="lougradouro"]').val(data.logradouro);
                  }
              }
          });
      } else {
          limpaFormulário();
      }
  });

});


// Componente gêneros
$(document).on('change', '.actionGenero',function(){
  var gen = $(this).val();
  var name = $(this).attr('id');
  if(gen == 'O'){
       $(this).attr('name','');
       $(this).parent().append('<input class="form-control shadow mb-3 bg-white rounded outroGenero" type="text" name="'+name+'">');
  }else{
       $('.outroGenero').remove();
       $(this).attr('name',name);
  }
})

// Gráficos

const grafico1 = new Chart(
  document.getElementById('grafico1'),
  configG1);

const grafico2 = new Chart(
  document.getElementById('grafico2'),
  configG2);




