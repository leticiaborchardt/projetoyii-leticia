<?php

use app\models\AreasComunsModel;
use app\models\CondominiosModel;
use phpDocumentor\Reflection\Types\This;
use yii\db\Migration;

/**
 * Class m220524_172302_add_table_areacomum
 */
class m220524_172302_add_table_areacomum extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(AreasComunsModel::tableName(), [
            'id' => $this->primaryKey()->unsigned(),
            'from_condominio' =>$this->integer()->notNull(),
            'nome' => $this->string(250)->notNull(),
            'metragem' => $this->string(100),
            'taxa' => $this->decimal(),
            'lotacaoMax' => $this->string(50)->notNull(),
            'dataCadastro' => $this->timestamp()->notNull()
        ]);

        $this->addForeignKey('fkArea_Condo', AreasComunsModel::tableName(), 'from_condominio', CondominiosModel::tableName(), 'id');    
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkArea_Condo', AreasComunsModel::tableName());
        $this->dropTable(AreasComunsModel::tableName());
    }
}
