<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220524_161710_add_table_administradora
 */
class m220524_161710_add_table_administradora extends Migration
{
      /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('t_administradora', [
            'id' => $this->primaryKey(),
            'nome' => Schema::TYPE_STRING . ' NOT NULL',
            'cnpj' => Schema::TYPE_STRING . ' NOT NULL',
            'dataCadastro' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('t_administradora');
    }
}
