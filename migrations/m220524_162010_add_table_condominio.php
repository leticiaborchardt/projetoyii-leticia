<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220524_162010_add_table_condominio
 */
class m220524_162010_add_table_condominio extends Migration
{
        /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('t_condominio', [
            'id' => $this->primaryKey(),
            'from_administradora' => Schema::TYPE_INTEGER . ' NOT NULL',
            'nomeCondominio' => Schema::TYPE_STRING . ' NOT NULL ',
            'qtdBlocos' => Schema::TYPE_INTEGER . ' NOT NULL',
            'lougradouro' => Schema::TYPE_STRING,
            'numero' => Schema::TYPE_INTEGER,
            'bairro' => Schema::TYPE_STRING,
            'cidade' => Schema::TYPE_STRING,
            'estado' => Schema::TYPE_STRING,
            'cep' => Schema::TYPE_STRING,
            'sindico' => Schema::TYPE_STRING,
            'dataCadastro' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
        ]);

        $this->createIndex('idx_adm', 't_condominio', 'from_administradora');

        $this->addForeignKey(
            'chAdm', 
            't_condominio', 
            'from_administradora', 
            't_administradora', 
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('chAdm', 't_condominio');
        $this->dropTable('t_condominio');
    }
}
