<?php

use app\models\BlocosModel;
use app\models\CondominiosModel;
use app\models\PetsModel;
use app\models\UnidadesModel;
use yii\db\Migration;

/**
 * Class m220524_173129_add_table_pets
 */
class m220524_173129_add_table_pets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(PetsModel::tableName(), [
            'id' => $this->primaryKey()->unsigned(),
            'from_condominio' =>$this->integer()->notNull(),
            'from_bloco' =>$this->integer()->notNull(),
            'from_unidade' => $this->integer()->unsigned()->notNull(),
            'nome' => $this->string(50)->notNull(),
            'especie' => $this->string(50)->notNull(),
            'raca' => $this->string(50)->notNull(),
            'porte' => "ENUM('P', 'M', 'G')",
            'cor' => $this->string(50)
        ]);

        $this->addForeignKey('fkPet_Condo', PetsModel::tableName(), 'from_condominio', CondominiosModel::tableName(), 'id' );
        $this->addForeignKey('fkPet_Bloco', PetsModel::tableName(), 'from_bloco', BlocosModel::tableName(), 'id' );
        $this->addForeignKey('fkPet_Unidade', PetsModel::tableName(), 'from_unidade', UnidadesModel::tableName(), 'id' );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkPet_Condo', PetsModel::tableName());
        $this->dropForeignKey('fkPet_Bloco', PetsModel::tableName());
        $this->dropForeignKey('fkPet_Unidade', PetsModel::tableName());
        $this->dropTable(PetsModel::tableName());
    }

}
