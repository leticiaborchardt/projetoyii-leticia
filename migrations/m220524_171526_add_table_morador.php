<?php

use app\models\BlocosModel;
use app\models\CondominiosModel;
use app\models\MoradoresModel;
use app\models\UnidadesModel;
use yii\db\Migration;

/**
 * Class m220524_171526_add_table_morador
 */
class m220524_171526_add_table_morador extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(MoradoresModel::tableName(), [
            'id' => $this->primaryKey()->unsigned(),
            'from_condominio' =>$this->integer()->notNull(),
            'from_bloco' =>$this->integer()->notNull(),
            'from_unidade' => $this->integer()->unsigned()->notNull(),
            'nome' =>$this->string(255)->notNull(),
            'cpf' => $this->string(11)->notNull(),
            'genero' => $this->string(50),
            'email' =>$this->string(255)->notNull(),
            'telefone' => $this->string(50),
            'dataCadastro' => $this->timestamp()->notNull()
        ]);

        $this->addForeignKey('fkMorador_Condo', MoradoresModel::tableName(), 'from_condominio', CondominiosModel::tableName(), 'id' );
        $this->addForeignKey('fkMorador_Bloco', MoradoresModel::tableName(), 'from_bloco', BlocosModel::tableName(), 'id' );
        $this->addForeignKey('fkMorador_Unidade', MoradoresModel::tableName(), 'from_unidade', UnidadesModel::tableName(), 'id' );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropForeignKey('fkMorador_Condo', MoradoresModel::tableName()); 
       $this->dropForeignKey('fkMorador_Bloco', MoradoresModel::tableName()); 
       $this->dropForeignKey('fkMorador_Unidade', MoradoresModel::tableName()); 
       $this->dropTable(MoradoresModel::tableName());
    }

}
