<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m220524_164530_add_table_bloco
 */
class m220524_164530_add_table_bloco extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('t_bloco', [
            'id' => $this->primaryKey(),
            'from_condominio' => Schema::TYPE_INTEGER . ' NOT NULL',
            'nomeBloco' => Schema::TYPE_STRING . ' NOT NULL',
            'andares' => Schema::TYPE_INTEGER . ' NOT NULL',
            'qtdUnidades' => Schema::TYPE_INTEGER . ' NOT NULL',
            'dataCadastro' => Schema::TYPE_TIMESTAMP . ' NOT NULL'
        ]);

        $this->createIndex('idx_cond', 't_bloco', 'from_condominio');

        $this->addForeignKey(
            'chCondo',
            't_bloco',
            'from_condominio',
            't_condominio',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('chCondo', 't_bloco');
       $this->dropTable('t_bloco');
    }

}
