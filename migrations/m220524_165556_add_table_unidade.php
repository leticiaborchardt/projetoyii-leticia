<?php

use app\models\BlocosModel;
use app\models\CondominiosModel;
use app\models\UnidadesModel;
use yii\db\Migration;

/**
 * Class m220524_165556_add_table_unidade
 */
class m220524_165556_add_table_unidade extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(UnidadesModel::tableName(), [
            'id' => $this->primaryKey()->unsigned(),
            'from_condominio' =>$this->integer()->notNull(),
            'from_bloco' =>$this->integer()->notNull(),
            'nomeUnidade' =>$this->string(100)->notNull(),
            'metragem' => $this->float()->notNull(),
            'qtdGaragem' => $this->integer(),
            'dataCadastro' => $this->timestamp()->notNull()
        ]);

        $this->addForeignKey('fkCondo', UnidadesModel::tableName(), 'from_condominio', CondominiosModel::tableName(), 'id' );
        $this->addForeignKey('fkBloco', UnidadesModel::tableName(), 'from_bloco', BlocosModel::tableName(), 'id' );
    
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkCondo', UnidadesModel::tableName());
        $this->dropForeignKey('fkBloco', UnidadesModel::tableName());
        $this->dropTable(UnidadesModel::tableName());
    }

}
