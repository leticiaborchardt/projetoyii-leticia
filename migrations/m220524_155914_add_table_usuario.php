<?php

use yii\db\Migration;
use yii\db\Schema;

class m220524_155914_add_table_usuario extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('t_usuario', [
            'id' => $this->primaryKey(),
            'nome' => Schema::TYPE_STRING . ' NOT NULL',
            'usuario' => Schema::TYPE_STRING . ' NOT NULL',
            'senha' => Schema::TYPE_STRING . ' NOT NULL',
            'dataCadastro' => Schema::TYPE_TIMESTAMP . ' NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('t_usuario');
    }

}
