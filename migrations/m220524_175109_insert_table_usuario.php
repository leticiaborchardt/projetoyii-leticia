<?php

use yii\db\Migration;

/**
 * Class m220524_175109_insert_table_usuario
 */
class m220524_175109_insert_table_usuario extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('t_usuario', [
            'usuario' => 'master',
            'senha' => '123',
            'nome' => 'Usuário Master'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('t_usuario', ['id' => '1']);
    }

}
