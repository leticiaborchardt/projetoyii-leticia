<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\BlocosModel;
use Yii;

class BlocosController extends Controller{
    public function actionListaBloco(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = (new \yii\db\Query())
        ->select(
            "bloco.id,
            cond.nomeCondominio,
            bloco.nomeBloco,
            bloco.andares,
            bloco.qtdUnidades,
            bloco.dataCadastro,
            bloco.from_condominio")
        ->from('t_bloco bloco')
        ->innerJoin('t_condominio cond', 'cond.id = bloco.from_condominio');

        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $blocos = $query->orderBy('nomeBloco')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('lista-bloco', [
            'blocos' => $blocos,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastroBloco(){
        return $this->render('cadastro-bloco');
    }

    public function actionRealizaCadastroBloco(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new BlocosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['blocos/lista-bloco']);
        }
        return $this->render('cadastro-bloco');
    }

    public static function listaBlocoSelect(){
        $query = BlocosModel::find();

        return $query->orderBy('nomeBloco')->all();
    }

    public static function listaBlocoSelectFrom($from){
        $query = BlocosModel::find();
        $data = $query->where(['from_condominio' => $from])->orderBy('nomeBloco')->all();
        return $data;
    }
    
    public function actionListaBlocoApi(){
        $request = \yii::$app->request;
        $query = BlocosModel::find();
        $data = $query->where(['from_condominio' => $request->post()])->orderBy('nomeBloco')->all();

        $dados = array();
        $i = 0;

        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nomeBloco'] = $d['nomeBloco'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionEditaBloco(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = BlocosModel::find();
            $blocos = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-bloco',[
            'edit' => $blocos
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = BlocosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
        
            if ($model->update()) {
                return $this->redirect(['blocos/lista-bloco',
                'myAlert' => [
                    'type' => 'success',
                    'msg' => 'Registro editado com sucesso!'
                ]
            ]);
            } else {
                return $this->redirect(['blocos/lista-bloco',
                'myAlert' => [
                    'type' => 'warning',
                    'msg' => 'Nenhum registro foi editado.'
                ]
            ]);
            }
        }
    }

    public function actionDeletaBloco(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = BlocosModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['blocos/lista-bloco',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['blocos/lista-bloco',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>