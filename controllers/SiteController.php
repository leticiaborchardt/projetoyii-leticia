<?
namespace app\controllers;

use app\models\AdministradorasModel;
use app\models\CondominiosModel;
use app\models\LoginForm;
use app\models\MoradoresModel;
use app\models\UnidadesModel;
use Yii;
use yii\web\Controller;
use yii\web\Response;


class SiteController extends Controller {

    //exibição de erros
    public function actions(){
        return [
            'error' =>[
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
        ];
    }


    public function actionIndex(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }

        $moradores = MoradoresModel::find()->count();
        $condominios = CondominiosModel::find()->count();
        $unidades = UnidadesModel::find()->count();
        $administradoras = AdministradorasModel::find()->count();

        return $this->render('home',[
            'moradores' => $moradores,
            'condominios' => $condominios,
            'unidades' => $unidades,
            'administradoras' => $administradoras
        ]);
    }


    public function actionLogin(){
        $this->layout = false;
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $request = \yii::$app->request;

        if($request->isPost){
            $identity = LoginForm::findOne(['usuario' => $request->post('usuario'), 'senha' => $request->post('senha')]);
            if($identity){
                Yii::$app->user->login($identity);
                return $this->redirect(['index']);
            }else{
                return $this->redirect(['login', 
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Login/senha incorretos!',
                    'redir' => 'index.php?r=site/login'
                    ]
                ]);
            }
        }
        return $this->render('login');
    }


    public function actionLogout(){
        yii::$app->user->logout();

        return $this->redirect(['site/login']);
    }



}

?>