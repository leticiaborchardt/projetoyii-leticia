<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\UnidadesModel;
use Yii;

class UnidadesController extends Controller{
    public function actionListaUnidade(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = (new \yii\db\Query())
        ->select(
            "uni.id,
            cond.nomeCondominio,
            bloco.nomeBloco,
            uni.nomeUnidade,
            uni.metragem,
            uni.qtdGaragem,
            uni.dataCadastro,
            uni.from_condominio,
            uni.from_bloco")
        ->from('t_unidade uni')
        ->innerJoin('t_condominio cond', 'cond.id = uni.from_condominio')
        ->innerJoin('t_bloco bloco', 'bloco.id = uni.from_bloco');

        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $unidades = $query->orderBy('nomeUnidade')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('lista-unidade', [
            'unidades' => $unidades,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastroUnidade(){
        return $this->render('cadastro-unidade');
    }

    public function actionRealizaCadastroUnidade(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new UnidadesModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['unidades/lista-unidade']);
        }
        return $this->render('cadastro-unidade');
    }

    public static function listaUnidadeSelect(){
        $query = UnidadesModel::find();

        return $query->orderBy('nomeUnidade')->all();
    }

    public static function listaUnidadeSelectFrom($from){
        $query = UnidadesModel::find();
        $data = $query->where(['from_bloco' => $from])->orderBy('nomeUnidade')->all();
        return $data;
    }


    public function actionListaUnidadeApi(){
        $request = \yii::$app->request;
        $query = UnidadesModel::find();
        $data = $query->where(['from_bloco' => $request->post()])->orderBy('nomeUnidade')->all();

        $dados = array();
        $i = 0;

        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nomeUnidade'] = $d['nomeUnidade'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionEditaUnidade(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = UnidadesModel::find();
            $unidades = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-unidade',[
            'edit' => $unidades
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = UnidadesModel::findOne($request->post('id'));
            $model->attributes = $request->post();
        
            if ($model->update()) {
                return $this->redirect(['unidades/lista-unidade',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro editado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['unidades/lista-unidade',
                    'myAlert' => [
                        'type' => 'warning',
                        'msg' => 'Nenhum registro foi editado.'
                    ]
                ]);
            }
        }
    }

    public function actionDeletaUnidade(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = UnidadesModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['unidades/lista-unidade',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['unidades/lista-unidade',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>