<?
namespace app\controllers;

use app\components\maskComponent;
use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\AdministradorasModel;

class AdministradorasController extends Controller{

    public function actionListaAdministradora(){

        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }

        $query = (new \yii\db\Query())
        ->select("*")
        ->from('t_administradora');

        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $administradoras = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('lista-administradora', [
            'administradoras' => $administradoras,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastroAdministradora(){
        return $this->render('cadastro-administradora');
    }

    public function actionRealizaCadastroAdministradora(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new AdministradorasModel();
            foreach($request->post() as $field=>$value){
                if($field == 'cnpj'){
                    $getMani[$field] = maskComponent::desMask($value);
                }else{
                    $getMani[$field] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['administradoras/lista-administradora']);
        }

        return $this->render('cadastro-administradora');
    }

    public static function listaAdministradoraSelect(){
        $query = AdministradorasModel::find();

        return $query->orderBy('nome')->all();
    }

    public function actionEditaAdministradora(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = AdministradorasModel::find();
            $administradoras = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-administradora',[
            'edit' => $administradoras
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = AdministradorasModel::findOne($request->post('id'));
            $model->attributes = $request->post();
        
            if ($model->update()) {
                return $this->redirect(['administradoras/lista-administradora',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro editado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['administradoras/lista-administradora',
                    'myAlert' => [
                        'type' => 'warning',
                        'msg' => 'Nenhum registro foi editado.'
                    ]
                ]);
            }
        }
    }

    public function actionDeletaAdministradora(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = AdministradorasModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['administradoras/lista-administradora',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['administradoras/lista-administradora',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>
