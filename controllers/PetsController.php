<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\PetsModel;
use Yii;

class PetsController extends Controller{
    public function actionListaPets(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }

        $query = (new \yii\db\Query())
        ->select(
            "pet.id,
            cond.nomeCondominio,
            bloco.nomeBloco,
            uni.nomeUnidade,
            pet.nome,
            pet.especie,
            pet.raca,
            pet.porte,
            pet.cor,
            pet.from_condominio,
            pet.from_bloco,
            pet.from_unidade")
        ->from('t_pets pet')
        ->innerJoin('t_condominio cond', 'cond.id = pet.from_condominio')
        ->innerJoin('t_bloco bloco', 'bloco.id = pet.from_bloco')
        ->innerJoin('t_unidade uni', 'uni.id = pet.from_unidade');


        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $pets = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('lista-pets', [
            'pets' => $pets,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastroPets(){
        return $this->render('cadastro-pets');
    }

    public function actionRealizaCadastroPets(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new PetsModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['pets/lista-pets']);
        }
        return $this->render('cadastro-pets');
    }


    public function actionEditaPets(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = PetsModel::find();
            $pets = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-pets',[
            'edit' => $pets
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = PetsModel::findOne($request->post('id'));
            $model->attributes = $request->post();
        
            if ($model->update()) {
                return $this->redirect(['pets/lista-pets',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro editado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['pets/lista-pets',
                    'myAlert' => [
                        'type' => 'warning',
                        'msg' => 'Nenhum registro foi editado.'
                    ]
                ]);
            }
        }
    }

    public function actionDeletaPets(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = PetsModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['pets/lista-pets',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['pets/lista-pets',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>