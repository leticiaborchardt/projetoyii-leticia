<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\ConselhosModel;

class ConselhosController extends Controller{
    public function actionListaConselho(){

        $query = (new \yii\db\Query())
        ->select(
            "con.id,
            con.nome,
            con.from_funcao,
            cond.nomeCondominio,
            con.dataCadastro,
            con.from_condominio")
        ->from('t_conselho con')
        ->innerJoin('t_condominio cond', 'cond.id = con.from_condominio');

        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $conselhos = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('lista-conselho', [
            'conselhos' => $conselhos,
            'paginacao' => $paginacao,
        ]);
    }
    
    public function actionCadastroConselho(){
        return $this->render('cadastro-conselho');
    }

    public function actionRealizaCadastroConselho(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new ConselhosModel();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['conselhos/lista-conselho']);
        }
        return $this->render('cadastro-conselho');
    }

    public function actionEditaConselho(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = ConselhosModel::find();
            $conselhos = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-conselho',[
            'edit' => $conselhos
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = ConselhosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
        
            if ($model->update()) {
                return $this->redirect(['conselhos/lista-conselho',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro editado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['conselhos/lista-conselho',
                    'myAlert' => [
                        'type' => 'warning',
                        'msg' => 'Nenhum registro foi editado.'
                    ]
                ]);
            }
        }
    }

    public function actionDeletaConselho(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = ConselhosModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['conselhos/lista-conselho',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['conselhos/lista-conselho',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>