<?
namespace app\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\User;
use Yii;

class UsuariosController extends Controller{
    public function actionListaUsuario(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
    
        $query = (new \yii\db\Query())
        ->select("*")
        ->from('t_usuario');


        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $usuarios = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('lista-usuario', [
            'usuarios' => $usuarios,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastroUsuario(){
        return $this->render('cadastro-usuario');
    }

    public function actionRealizaCadastroUsuario(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new User();
            $model->attributes = $request->post();
            $model->save();
            return $this->redirect(['usuarios/lista-usuario']);
        }
        return $this->render('cadastro-usuario');
    }

    public function actionEditaUsuario(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = User::find();
            $usuarios = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-usuario',[
            'edit' => $usuarios
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = User::findOne($request->post('id'));
            $model->attributes = $request->post();
        
            if ($model->update()) {
                return $this->redirect(['usuarios/lista-usuario',
                'myAlert' => [
                    'type' => 'success',
                    'msg' => 'Registro editado com sucesso!'
                ]
            ]);
            } else {
                return $this->redirect(['usuarios/lista-usuario',
                    'myAlert' => [
                        'type' => 'warning',
                        'msg' => 'Nenhum registro foi editado.'
                    ]
                ]);
            }
        }
    }

    public function actionDeletaUsuario(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = User::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['usuarios/lista-usuario',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['usuarios/lista-usuario',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>