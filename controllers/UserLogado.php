<?
namespace app\controllers;

use Yii;
use yii\base\Controller;

Class UserLogado extends Controller {
    
    public function isLogado(){
        if(yii::$app->user->isGuest){
            $this->$this->redirect(['site/login']);
        }
    }
}
?>