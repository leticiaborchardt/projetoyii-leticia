<?
namespace app\controllers;

use app\components\maskComponent;
use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\AreasComunsModel;

class AreasComunsController extends Controller{

    public function actionListaArea(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = (new \yii\db\Query())
        ->select(
            "area.id,
            cond.nomeCondominio,
            area.nome,
            area.metragem,
            area.taxa,
            area.lotacaoMax,
            area.dataCadastro,
            area.from_condominio")
        ->from('t_areacomum area')
        ->innerJoin('t_condominio cond', 'cond.id = area.from_condominio');

        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $areas = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('lista-area', [
            'areas' => $areas,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastroArea(){
        return $this->render('cadastro-area');
    }

    public function actionRealizaCadastroArea(){
        $request = \yii::$app->request;

        if($request->isPost){
            $model = new AreasComunsModel();
            foreach($request->post() as $field=>$value){
                if($field == 'taxa'){
                    $getMani[$field] = maskComponent::money($value);
                }else{
                    $getMani[$field] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['areas-comuns/lista-area']);
            }
        return $this->render('cadastro-area');
    }

    public static function listaAreaSelect(){
        $query = AreasComunsModel::find();

        return $query->orderBy('nome')->all();
    }

    public static function listaAreaSelectFrom($from){
        $query = AreasComunsModel::find();
        $data = $query->where(['from_areacomum' => $from])->orderBy('nome')->all();
        return $data;
    }

    public function actionListaAreaApi(){
        $request = \yii::$app->request;
        $query = AreasComunsModel::find();
        $data = $query->where(['from_areacomum' => $request->post()])->orderBy('nome')->all();

        $dados = array();
        $i = 0;

        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nome'] = $d['nome'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionEditaArea(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = AreasComunsModel::find();
            $areas = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-area',[
            'edit' => $areas
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = AreasComunsModel::findOne($request->post('id'));
            $model->attributes = $request->post();
        
            if ($model->update()) {
                return $this->redirect(['areas-comuns/lista-area',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro editado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['areas-comuns/lista-area',
                    'myAlert' => [
                        'type' => 'warning',
                        'msg' => 'Nenhum registro foi editado.'
                    ]
                ]);
            }
        }
    }

    public function actionDeletaArea(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = AreasComunsModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['areas-comuns/lista-area',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['areas-comuns/lista-area',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>
