<?
namespace app\controllers;

use app\components\maskComponent;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\MoradoresModel;
use Yii;

class MoradoresController extends Controller{
    public function actionListaMorador(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        
        $query = (new \yii\db\Query())
        ->select(
            "mor.id,
            cond.nomeCondominio,
            bloco.nomeBloco,
            uni.nomeUnidade,
            mor.nome,
            mor.cpf,
            mor.email,
            mor.telefone,
            mor.dataCadastro,
            mor.from_condominio,
            mor.from_bloco,
            mor.from_unidade")
        ->from('t_morador mor')
        ->innerJoin('t_condominio cond', 'cond.id = mor.from_condominio')
        ->innerJoin('t_bloco bloco', 'bloco.id = mor.from_bloco')
        ->innerJoin('t_unidade uni', 'uni.id = mor.from_unidade');

        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $moradores = $query->orderBy('nome')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

            
        return $this->render('lista-morador', [
            'moradores' => $moradores,
            'paginacao' => $paginacao,
        ]);
    }
    
    public function actionCadastroMorador(){
        return $this->render('cadastro-morador');
    }



    public function actionRealizaCadastroMorador(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new MoradoresModel();
            foreach($request->post() as $field=>$value){
                if([$field == 'cpf', $field == 'tel']){
                    $getMani[$field] = maskComponent::desMask($value);
                }
                else{
                    $getMani[$field] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['moradores/lista-morador']);
        }
        return $this->render('cadastro-morador');
    }



    public static function listaMoradorSelect(){
        $query = MoradoresModel::find();

        return $query->orderBy('nome')->all();
    }

    public static function listaMoradorSelectFrom($from){
        $query = MoradoresModel::find();
        $data = $query->where(['from_unidade' => $from])->orderBy('nome')->all();
        return $data;
    }

    public function actionListaMoradorApi(){
        $request = \yii::$app->request;
        $query = MoradoresModel::find();
        $data = $query->where(['from_unidade' => $request->post()])->orderBy('nome')->all();

        $dados = array();
        $i = 0;

        foreach($data as $d){
            $dados[$i]['id'] = $d['id'];
            $dados[$i]['nome'] = $d['nome'];
            $i++;
        }
        return json_encode($dados);
    }

    public function actionEditaMorador(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = MoradoresModel::find();
            $moradores = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-morador',[
            'edit' => $moradores
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = MoradoresModel::findOne($request->post('id'));
            $model->attributes = $request->post();
           
            if ($model->update()) {
                return $this->redirect(['moradores/lista-morador',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro editado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['moradores/lista-morador',
                    'myAlert' => [
                        'type' => 'warning',
                        'msg' => 'Nenhum registro foi editado.'
                    ]
                ]);
            }
        }
    }

    public function actionDeletaMorador(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = MoradoresModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['moradores/lista-morador',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['moradores/lista-morador',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>