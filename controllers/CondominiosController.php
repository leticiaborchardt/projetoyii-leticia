<?
namespace app\controllers;

use app\components\maskComponent;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\CondominiosModel;
use Yii;

class CondominiosController extends Controller{
    public function actionListaCondominio(){
        if(Yii::$app->user->isGuest){
            $this->redirect(['site/login']);
        }
        $query = (new \yii\db\Query())
        ->select([
        "cond.id",
        "adm.nome",
        "cond.nomeCondominio",
        "cond.qtdBlocos",
        "cond.lougradouro",
        "cond.numero",
        "cond.bairro",
        "cond.cidade",
        "cond.estado",
        "cond.cep",
        "cond.sindico",
        "cond.dataCadastro",
        "cond.from_administradora"])
        ->from('t_condominio cond')
        ->innerJoin('t_administradora adm', 'adm.id = cond.from_administradora');

        $paginacao= new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $condominios = $query->orderBy('nomeCondominio')
            ->offset($paginacao->offset)
            ->limit($paginacao->limit)
            ->all();

        return $this->render('lista-condominio', [
            'condominios' => $condominios,
            'paginacao' => $paginacao,
        ]);
    }

    public function actionCadastroCondominio(){
        return $this->render('cadastro-condominio');
    }

    public function actionRealizaCadastroCondominio(){
        $request = \yii::$app->request;

        if ($request->isPost) {
            $model = new CondominiosModel();
            foreach($request->post() as $field=>$value){
                if($field == 'cep'){
                    $getMani[$field] = maskComponent::desMask($value);
                }else{
                    $getMani[$field] = $value;
                }
            }
            $model->attributes = $getMani;
            $model->save();
            return $this->redirect(['condominios/lista-condominio']);
        }
        return $this->render('cadastro-condominio');
    }

    public static function listaCondominioSelect(){
        $query = CondominiosModel::find();

        return $query->orderBy('nomeCondominio')->all();
    }

    public function actionEditaCondominio(){
        $this->layout = false;
        $request = \yii::$app->request;
        if ($request->isGet) {
            $query = CondominiosModel::find();
            $condominios = $query->where(['id' => $request->get()])->one();
        }
        return $this->render('edita-condominio',[
            'edit' => $condominios
        ]);
    }

    public function actionRealizaEdicao(){
        $request = \yii::$app->request;
        
        if ($request->isPost) {

            $model = CondominiosModel::findOne($request->post('id'));
            $model->attributes = $request->post();
        
            if ($model->update()) {
                return $this->redirect(['condominios/lista-condominio',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro editado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['condominios/lista-condominio',
                    'myAlert' => [
                        'type' => 'warning',
                        'msg' => 'Nenhum registro foi editado.'
                    ]
                ]);
            }
        }
    }

    public function actionDeletaCondominio(){
        $request = \yii::$app->request;

        if ($request->isGet) {
            $model = CondominiosModel::findOne($request->get('id'));
            if ($model->delete()) {
                return $this->redirect(['condominios/lista-condominio',
                    'myAlert' => [
                        'type' => 'success',
                        'msg' => 'Registro deletado com sucesso!'
                    ]
                ]);
            } else {
                return $this->redirect(['condominios/lista-condominio',
                'myAlert' => [
                    'type' => 'danger',
                    'msg' => 'Não foi possível deletar o registro.'
                    ]
                ]);
            }
        }
    }
}
?>