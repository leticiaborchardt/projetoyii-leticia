<?
namespace app\models;

use yii\db\ActiveRecord;

class BlocosModel extends ActiveRecord{

    public static function tableName(){
        return 't_bloco';
    }

    public function rules(){
        return [
            [['from_condominio', 
            'nomeBloco', 
            'andares', 
            'qtdUnidades'], 
            
            'required']
        ];
    }
};
?>
