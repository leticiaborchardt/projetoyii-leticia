<?
namespace app\models;

use yii\db\ActiveRecord;

class AdministradorasModel extends ActiveRecord{

    public static function tableName(){
        return 't_administradora';
    }
    
    public function rules(){
        return [
            [['nome', 'cnpj'], 'required']
        ];
    }
};
?>
