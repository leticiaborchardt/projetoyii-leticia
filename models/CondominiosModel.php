<?
namespace app\models;

use yii\db\ActiveRecord;

class CondominiosModel extends ActiveRecord{

    public static function tableName(){
        return 't_condominio';
    }

    public function rules(){
        return [
            [['from_administradora', 
            'nomeCondominio', 
            'qtdBlocos',
            'sindico'],'required'],

            [['lougradouro',
            'numero',
            'bairro',
            'cidade',
            'estado',
            'cep'], 'default']
        ];
    }
};

?>