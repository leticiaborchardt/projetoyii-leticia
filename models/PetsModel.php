<?
namespace app\models;

use yii\db\ActiveRecord;

class PetsModel extends ActiveRecord{

    public static function tableName(){
        return 't_pets';
    }

    public function rules(){
        return [
            [['from_condominio', 
            'from_bloco', 
            'from_unidade', 
            'nome',
            'especie',
            'raca',
            'porte',
            'cor'], 
            
            'required']
        ];
    }
};
?>