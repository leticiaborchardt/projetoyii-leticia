<?
namespace app\models;

use yii\db\ActiveRecord;

class MoradoresModel extends ActiveRecord{

    public static function tableName(){
        return 't_morador';
    }

    public function rules(){
        return [
            [[
            'from_condominio', 
            'from_bloco',
            'from_unidade', 
            'nome', 
            'cpf',
            'email'], 'required'],

            [['telefone', 
            'genero'], 'default']
        ];
    }
};
?>