<?
namespace app\models;

use yii\db\ActiveRecord;

class AreasComunsModel extends ActiveRecord{

    public static function tableName(){
        return 't_areacomum';
    }

    public function rules(){
        return [
            [['from_condominio', 
            'nome', 
            'metragem', 
            'taxa',
            'lotacaoMax'], 
            
            'required']
        ];
    }
};
?>