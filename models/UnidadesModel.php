<?
namespace app\models;

use yii\db\ActiveRecord;

class UnidadesModel extends ActiveRecord{

    public static function tableName(){
        return 't_unidade';
    }

    public function rules(){
        return [
            [['from_condominio', 
            'from_bloco', 
            'nomeUnidade', 
            'metragem',
            'qtdGaragem'], 
            
            'required']
        ];
    }
};
?>