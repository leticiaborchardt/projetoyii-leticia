<?
namespace app\models;

use yii\db\ActiveRecord;

class ConselhosModel extends ActiveRecord{

    public static function tableName(){
        return 't_conselho';
    }

    public function rules(){
        return [
            [['from_condominio', 
            'nome', 
            'from_funcao'],
            
            'required']
        ];
    }
};
?>