<?
namespace app\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

Class User extends ActiveRecord implements IdentityInterface{

    public static function tableName(){
        return 't_usuario';
    }


    // procura a instancia da classe usando o id do usuário especificado. Usado para manter o status de login via sessão.
    public static function findIdentity($id){
        return static::findOne($id);
    }


    // usa o token de acesso informado para autenticar um usuario por um unico token secreto.
    public static function findIdentityByAccessToken($token, $type = null){
        return null;
    } 
    
    
    // retorna o id do usuario representado por essa instância da classe de identidade.
    public function getId(){
        return $this->id;
    }


    // retorna uma chave para verificar login via cookie. É mantida e será comparada com a informação do lado servidor para testar a validade do cookie.
    public function getAuthKey(){
        return null;
    }


    // implementa a lógica de verificação da chave de login via cookie;
    public function validateAuthKey($authKey){
        return null;
    }

};
?>