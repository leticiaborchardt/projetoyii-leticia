<?
namespace app\modules\api\controllers;

use app\models\User;
use Exception;
use Yii;
use yii\web\Controller;

Class LoginController extends Controller {

    public function actionIndex(){
        
        $request = \yii::$app->request;

        try {
            if ($request->isPost){
                $identity = User::findOne(['usuario' => $request->post('usuario'), 'senha' => $request->post('senha')]);
                if ($identity) {
                    Yii::$app->user->login($identity);
                    $dados['endPoint']['status'] = 'success';
                    return json_encode($dados);
                } else {
                    $dados['endPoint']['status'] = 'noLogin';
                    $dados['endPoint']['msg'] = 'Os dados de login e senha não conferem';
                    return json_encode($dados);
                }
            }
        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noLogin';
            $dados['endPoint']['msg'] = $th;
            return json_encode($dados);
        }
    }
}
?>