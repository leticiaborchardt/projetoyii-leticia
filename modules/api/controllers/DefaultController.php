<?
namespace app\modules\api\controllers;

use yii\web\Controller;

Class DefaultController extends Controller {
    
    public function actionIndex(){
        $this->layout = false;
        return $this->render('index');
    }

    public function actionGetTokenPost(){
        $fieldName = \yii::$app->request->csrfParam;
        $tokenValue = \yii::$app->request->csrfToken;

        if ($fieldName && $tokenValue) {
            $result = [
                'field' => $fieldName,
                'token' => $tokenValue
            ];
            return json_encode($result);
        } else {
            return false;
        }
    }
}
?>