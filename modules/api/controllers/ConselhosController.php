<?
namespace app\modules\api\controllers;

use app\models\CondominiosModel;
use app\models\ConselhosModel;
use Exception;
use yii\web\Controller;

Class ConselhosController extends Controller{

    public function behaviors() {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    // restrict access to
                    'Origin' => ['http://localhost', 'https://localhost'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Request-Headers' => ['*'],
                    // Allow credentials (cookies, authorization headers, etc.) to be exposed to the browser
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],
    
            ],
        ];
    }

    //retorna todos os dados
    public function actionGetAll(){

        $qry = (new \yii\db\Query())
        ->select(
            "con.id,
            con.nome,
            con.from_funcao,
            cond.nomeCondominio,
            con.dataCadastro,
            con.from_condominio")
        ->from(ConselhosModel::tableName() . ' con')
        ->innerJoin(CondominiosModel::tableName() . ' cond', 'cond.id = con.from_condominio');

        $data = $qry->orderBy('nome')->all();
        $dados = [];
        $i = 0;

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            $dados['totalResults'] = $qry->count();
            foreach ($data as $d) {
                foreach ($d as $ch=>$r){
                    $dados['resultSet'][$i][$ch] = $r;
                }
                $i++;
            }
        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = "Não existem dados para este consumo.";
        }
        return json_encode($dados);
    }

    //retorna apenas o dado requerido
    public function actionGetOne(){

        $qry = (new \yii\db\Query())
        ->select(
            "con.id,
            con.nome,
            con.from_funcao,
            cond.nomeCondominio,
            con.dataCadastro,
            con.from_condominio")
        ->from(ConselhosModel::tableName() . ' con')
        ->innerJoin(CondominiosModel::tableName() . ' cond', 'cond.id = con.from_condominio');

        $request = \yii::$app->request;
        $d = $qry->where(['con.id' => $request->get('id')])->one();

        if ($qry->count() > 0) {
            $dados['endPoint']['status'] = 'success';
            foreach ($d as $ch=>$r){
                $dados['resultSet'][0][$ch] = $r;
            }

        } else {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = "Não existem dados para este consumo.";
        }

        return json_encode($dados);
    }


    //adicionar um novo registro
    public function actionRegisterConse(){
        $request = \yii::$app->request;

        try {
            if ($request->isPost) { 
                $dados = [];
                $model = new ConselhosModel();
                $model->attributes = $request->post();
                if($model->save()){
                    $dados['endPoint']['status'] = 'success';
                    $dados['endPoint']['msg'] = 'Registro inserido com sucesso!';
                } else {
                    $dados['endPoint']['status'] = 'noData';
                    $dados['endPoint']['msg'] = "Não foi possível executar essa operação";
                }

                return json_encode($dados);
            } 
        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = "Registro não inserido.";

            return json_encode($dados);
        }
    }

    //editar um registro
    public function actionEditConse(){
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $model = ConselhosModel::findOne($request->post('id'));
                $model->attributes = $request->post();
                $model->update();

                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro editado com sucesso!';

                return json_encode($dados);
            } 
        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['msg'] = "O registro não pode ser editado";

            return json_encode($dados);
        }
    }


    //remove um registro
    public function actionDelete(){
        $request = \yii::$app->request;

        try {
            if ($request->isPost) {
                $model = ConselhosModel::findOne($request->post('id'));
                $model->delete();

                $dados = [];
                $dados['endPoint']['status'] = 'success';
                $dados['endPoint']['msg'] = 'Registro removido com sucesso!';

                return json_encode($dados);
            } 
        } catch (Exception $th) {
            $dados['endPoint']['status'] = 'noData';
            $dados['endPoint']['error'] = $th;
            $dados['endPoint']['msg'] = "O registro não pode ser removido.";

            return json_encode($dados);
        }
    }

}
?>