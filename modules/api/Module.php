<?
namespace app\modules\api;

Class Module extends \yii\base\Module {
    public $controllerNamespace = 'app\modules\api\controllers';

    public function init(){
        parent::init();
    }
}
?>