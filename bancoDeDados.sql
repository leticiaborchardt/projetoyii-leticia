-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.24-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para bd_sistema
CREATE DATABASE IF NOT EXISTS `bd_sistema` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `bd_sistema`;

-- Copiando estrutura para tabela bd_sistema.t_administradora
CREATE TABLE IF NOT EXISTS `t_administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '0',
  `cnpj` varchar(50) NOT NULL DEFAULT '0',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_administradora: ~2 rows (aproximadamente)
DELETE FROM `t_administradora`;
INSERT INTO `t_administradora` (`id`, `nome`, `cnpj`, `dataCadastro`) VALUES
	(37, 'LB´s Administradora', '08760633333333', '2022-04-26 19:05:20'),
	(38, 'Phoenix Condomínios', '47224102000154', '2022-04-13 14:03:49');

-- Copiando estrutura para tabela bd_sistema.t_areacomum
CREATE TABLE IF NOT EXISTS `t_areacomum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) DEFAULT NULL,
  `nome` varchar(250) NOT NULL DEFAULT '',
  `metragem` varchar(250) DEFAULT '-',
  `taxa` decimal(20,2) DEFAULT NULL,
  `lotacaoMax` varchar(250) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chACondominio` (`from_condominio`),
  CONSTRAINT `chACondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_areacomum: ~2 rows (aproximadamente)
DELETE FROM `t_areacomum`;
INSERT INTO `t_areacomum` (`id`, `from_condominio`, `nome`, `metragem`, `taxa`, `lotacaoMax`, `dataCadastro`) VALUES
	(4, 32, 'Área de Festas', '500', 0.00, '66', '2022-05-08 21:16:21'),
	(5, 35, 'Quiosque', '200', 50.00, '50', '2022-05-08 20:13:00');

-- Copiando estrutura para tabela bd_sistema.t_bloco
CREATE TABLE IF NOT EXISTS `t_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `nomeBloco` varchar(50) NOT NULL DEFAULT '0',
  `andares` int(11) NOT NULL DEFAULT 0,
  `qtdUnidades` int(11) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chBCondominio` (`from_condominio`),
  CONSTRAINT `chBCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_bloco: ~8 rows (aproximadamente)
DELETE FROM `t_bloco`;
INSERT INTO `t_bloco` (`id`, `from_condominio`, `nomeBloco`, `andares`, `qtdUnidades`, `dataCadastro`) VALUES
	(24, 35, 'Bloco A', 3, 2, '2022-04-13 14:09:14'),
	(25, 35, 'Bloco B', 3, 2, '2022-04-13 14:09:27'),
	(26, 34, 'Bloco 01', 4, 2, '2022-04-22 18:34:31'),
	(27, 34, 'Bloco 02', 4, 2, '2022-04-13 14:10:21'),
	(28, 32, 'Bloco Playa', 3, 3, '2022-04-13 14:10:40'),
	(29, 32, 'Bloco Ville', 4, 3, '2022-04-13 14:10:52'),
	(30, 33, 'Bloco A', 2, 2, '2022-04-13 14:11:16'),
	(31, 33, 'Bloco B', 3, 2, '2022-04-13 14:11:29');

-- Copiando estrutura para tabela bd_sistema.t_condominio
CREATE TABLE IF NOT EXISTS `t_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_administradora` int(11) NOT NULL,
  `nomeCondominio` varchar(255) NOT NULL DEFAULT '0',
  `qtdBlocos` int(11) NOT NULL DEFAULT 0,
  `lougradouro` varchar(255) DEFAULT '0',
  `numero` int(8) DEFAULT 0,
  `bairro` varchar(255) DEFAULT '0',
  `cidade` varchar(255) DEFAULT '0',
  `estado` varchar(255) DEFAULT '0',
  `cep` varchar(8) DEFAULT '0',
  `sindico` varchar(255) NOT NULL DEFAULT '0',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chAdm` (`from_administradora`) USING BTREE,
  CONSTRAINT `chAdm` FOREIGN KEY (`from_administradora`) REFERENCES `t_administradora` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_condominio: ~4 rows (aproximadamente)
DELETE FROM `t_condominio`;
INSERT INTO `t_condominio` (`id`, `from_administradora`, `nomeCondominio`, `qtdBlocos`, `lougradouro`, `numero`, `bairro`, `cidade`, `estado`, `cep`, `sindico`, `dataCadastro`) VALUES
	(32, 37, 'Residencial Villa Nova', 2, 'Rua Fernando de Noronha', 1566, 'Centro', 'Rio de Janeiro', 'RJ', '89120000', 'Ana Paula Santos', '2022-04-13 14:04:43'),
	(33, 37, 'Vale das Flores Condo', 2, 'Rua Belém', 112, 'Terezinha', 'Parazinho', 'PA', '45789005', 'Rodrigo Vieira Santos', '2022-04-13 14:06:11'),
	(34, 38, 'Residencial Quatro Estações', 2, 'Rua Figueira', 1987, 'Estação', 'Porto Alegre', 'ES', '45963551', 'Letícia Borchardt', '2022-04-13 14:07:22'),
	(35, 38, 'Condomínio Bela Vista', 2, 'Rua 7 de Setembro', 145, 'Centro', 'Timbó', 'SC', '00123005', 'José Vinnicius', '2022-04-22 18:33:28');

-- Copiando estrutura para tabela bd_sistema.t_conselho
CREATE TABLE IF NOT EXISTS `t_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '0',
  `from_funcao` enum('Subsíndico','Conselheiro') NOT NULL,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCCondominio` (`from_condominio`),
  CONSTRAINT `chCCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_conselho: ~8 rows (aproximadamente)
DELETE FROM `t_conselho`;
INSERT INTO `t_conselho` (`id`, `from_condominio`, `nome`, `from_funcao`, `dataCadastro`) VALUES
	(13, 35, 'Robert Melo Amarante', 'Subsíndico', '2022-04-13 16:02:57'),
	(14, 35, 'Anastasia Cabral Loio', 'Subsíndico', '2022-04-22 19:35:52'),
	(15, 34, 'Stefany Girão Feijó', 'Subsíndico', '2022-04-13 16:03:36'),
	(16, 34, 'Orlando Ribas Quadros', 'Conselheiro', '2022-04-13 16:03:44'),
	(17, 32, 'Dinarte Anlicoara Silveira', 'Subsíndico', '2022-04-13 16:04:02'),
	(18, 32, 'Kristian Ruela Milheirão', 'Conselheiro', '2022-04-13 16:04:09'),
	(19, 33, 'Mafalda Damasceno Poças', 'Subsíndico', '2022-04-13 16:04:17'),
	(20, 33, 'Nuno Cardim Machado', 'Conselheiro', '2022-04-13 16:04:25');

-- Copiando estrutura para tabela bd_sistema.t_morador
CREATE TABLE IF NOT EXISTS `t_morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `from_bloco` int(11) NOT NULL DEFAULT 0,
  `from_unidade` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(50) NOT NULL DEFAULT '',
  `genero` varchar(50) DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefone` varchar(50) DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chMBloco` (`from_bloco`),
  KEY `chMCondominio` (`from_condominio`),
  KEY `chMUnidade` (`from_unidade`),
  CONSTRAINT `chMBloco` FOREIGN KEY (`from_bloco`) REFERENCES `t_bloco` (`id`),
  CONSTRAINT `chMCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`),
  CONSTRAINT `chMUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `t_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_morador: ~18 rows (aproximadamente)
DELETE FROM `t_morador`;
INSERT INTO `t_morador` (`id`, `from_condominio`, `from_bloco`, `from_unidade`, `nome`, `cpf`, `genero`, `email`, `telefone`, `dataCadastro`) VALUES
	(41, 35, 24, 35, 'Evelina Espinosa Fitas', '83836707020', 'F', 'sheba7041@uorak.com', '47988887777', '2022-04-13 14:18:04'),
	(42, 35, 24, 36, 'Anaisa Magalhães', '20006900012', 'F', 'mikaela@email.com', '47988588569', '2022-04-13 19:54:28'),
	(43, 35, 25, 37, 'Lian Faustino Cortesão', '78978978998', 'M', 'marvintabosa@email.com', NULL, '2022-04-13 14:19:22'),
	(44, 35, 25, 38, 'Máximo Marins Camacho', '45656456487', 'M', 'mar@email.com', '47914518888', '2022-04-13 14:20:29'),
	(45, 34, 26, 40, 'Johnny Vilas Boas Morgado', '45678912305', 'M', 'Johnny@gmail.com', '47874125634', '2022-04-13 14:21:32'),
	(46, 34, 26, 39, 'Cláudia Cabreira Barrela', '45622578910', 'F', 'luana@a.com', '479888888888', '2022-04-13 19:05:07'),
	(47, 34, 27, 42, 'Graça Beiriz Palha', '45678912300', 'Cis', 'janiceacelar@gmail.com', '48909999999', '2022-04-13 14:23:58'),
	(48, 34, 27, 41, 'Osvaldo Robalo Laureano', '00241156336', 'M', 'osvaldo@email.com', NULL, '2022-04-13 14:24:36'),
	(49, 32, 28, 44, 'Rossana Caiado Cedraz', '12345678912', 'F', 'aa@a.com', NULL, '2022-04-13 14:29:34'),
	(50, 32, 28, 45, 'Lyah Varão Brasil', '00100500412', 'F', 'luana@a.com', NULL, '2022-04-13 14:29:54'),
	(51, 32, 29, 46, 'Christopher Brum Basílio', '45687000812', 'M', 'christopher@a.com', '4899999999', '2022-04-13 19:07:50'),
	(52, 32, 29, 47, 'Taíssa Faustino Guerra', '45600200698', 'F', 'celar@gmail.com', '479888888888', '2022-04-13 14:30:44'),
	(53, 33, 30, 49, 'Rui Casalinho Garcia', '10025648898', 'M', 'aa@a.com', NULL, '2022-04-13 14:46:41'),
	(54, 33, 30, 50, 'Nara Marques Batata', '12345600021', 'F', 'luana@a.com', NULL, '2022-04-13 14:47:02'),
	(55, 33, 31, 52, 'Laryssa Vargas Braga', '160081630-01', 'F', 'bosa@email.com', NULL, '2022-04-13 14:47:31'),
	(56, 33, 31, 51, 'Raphael Cavalheiro Frois', '45551215401', 'M', 'ar@gmail.com', NULL, '2022-04-13 14:47:58'),
	(63, 35, 25, 37, 'Alma Eiró Conceição', '16315749413', 'F', 'alma@gmail.com.br', '16314658416', '2022-04-29 12:01:27'),
	(65, 35, 24, 36, 'NicolasCauanNardelli', '06232678966', 'M', 'nickcauan@hotmailcom', '47996535441', '2022-05-08 19:48:43');

-- Copiando estrutura para tabela bd_sistema.t_pets
CREATE TABLE IF NOT EXISTS `t_pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `from_bloco` int(11) NOT NULL,
  `from_unidade` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `especie` varchar(50) NOT NULL,
  `raca` varchar(50) NOT NULL,
  `porte` enum('P','M','G') NOT NULL,
  `cor` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chPCondo` (`from_condominio`),
  KEY `chPBloco` (`from_bloco`),
  KEY `chPUnidade` (`from_unidade`),
  CONSTRAINT `chPBloco` FOREIGN KEY (`from_bloco`) REFERENCES `t_bloco` (`id`),
  CONSTRAINT `chPCondo` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`),
  CONSTRAINT `chPUnidade` FOREIGN KEY (`from_unidade`) REFERENCES `t_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_pets: ~4 rows (aproximadamente)
DELETE FROM `t_pets`;
INSERT INTO `t_pets` (`id`, `from_condominio`, `from_bloco`, `from_unidade`, `nome`, `especie`, `raca`, `porte`, `cor`) VALUES
	(2, 33, 30, 50, 'Fifi', 'Gato', 'Siamês', 'P', 'Branco'),
	(3, 34, 26, 40, 'Stitch', 'Pássaro', 'Papagaio', 'P', 'Verde'),
	(4, 33, 31, 51, 'Otávio', 'Cachorro', 'Corgi', 'M', 'Bege'),
	(5, 32, 28, 44, 'Laila', 'Cachorro', 'Pinscher', 'P', 'Caramelo');

-- Copiando estrutura para tabela bd_sistema.t_unidade
CREATE TABLE IF NOT EXISTS `t_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_condominio` int(11) NOT NULL DEFAULT 0,
  `from_bloco` int(11) NOT NULL DEFAULT 0,
  `nomeUnidade` varchar(255) NOT NULL DEFAULT '0',
  `metragem` float NOT NULL DEFAULT 0,
  `qtdGaragem` int(11) DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chUBloco` (`from_bloco`),
  KEY `chUCondominio` (`from_condominio`),
  CONSTRAINT `chUBloco` FOREIGN KEY (`from_bloco`) REFERENCES `t_bloco` (`id`),
  CONSTRAINT `chUCondominio` FOREIGN KEY (`from_condominio`) REFERENCES `t_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_unidade: ~16 rows (aproximadamente)
DELETE FROM `t_unidade`;
INSERT INTO `t_unidade` (`id`, `from_condominio`, `from_bloco`, `nomeUnidade`, `metragem`, `qtdGaragem`, `dataCadastro`) VALUES
	(35, 35, 24, 'Apto 101', 100, 1, '2022-04-13 14:11:48'),
	(36, 35, 24, 'Apto 102', 120, 2, '2022-04-13 14:11:59'),
	(37, 35, 25, 'Apto 201', 120, 2, '2022-04-13 14:12:10'),
	(38, 35, 25, 'Apto 202', 150, 2, '2022-04-13 14:12:27'),
	(39, 34, 26, 'Apto 301', 100, 1, '2022-04-13 14:12:45'),
	(40, 34, 26, 'Apto 103', 120, 1, '2022-04-13 14:12:55'),
	(41, 34, 27, 'Apto 220', 120, 1, '2022-04-13 14:13:12'),
	(42, 34, 27, 'Apto 101', 120, 2, '2022-04-13 14:13:30'),
	(44, 32, 28, 'Apto 500', 98, 1, '2022-04-13 14:14:22'),
	(45, 32, 28, 'Apto 601', 87, 0, '2022-04-13 14:14:35'),
	(46, 32, 29, 'Apto 201', 125, 1, '2022-04-13 14:14:49'),
	(47, 32, 29, 'Apto 890', 158, 1, '2022-04-13 14:15:01'),
	(49, 33, 30, 'Apto 101', 110, 1, '2022-04-13 14:15:45'),
	(50, 33, 30, 'Apto 201', 159, 1, '2022-04-13 14:15:54'),
	(51, 33, 31, 'Sala 12', 250, 3, '2022-04-13 14:16:14'),
	(52, 33, 31, 'Sala 04', 200, 0, '2022-04-13 14:16:28');

-- Copiando estrutura para tabela bd_sistema.t_usuario
CREATE TABLE IF NOT EXISTS `t_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `usuario` varchar(255) NOT NULL DEFAULT '',
  `senha` varchar(255) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela bd_sistema.t_usuario: ~2 rows (aproximadamente)
DELETE FROM `t_usuario`;
INSERT INTO `t_usuario` (`id`, `nome`, `usuario`, `senha`, `dataCadastro`) VALUES
	(20, 'Letícia Borchardt', 'leticia', '123', '2022-04-13 16:29:26'),
	(24, 'Usuário', 'user', '202cb962ac59075b964b07152d234b70', '2022-04-28 13:38:15');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
